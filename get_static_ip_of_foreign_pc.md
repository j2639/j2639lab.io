Mit 
```
sudo tcpdump -i $INTERFACE -nevvv not ether src $MyMACAddress
```
kann man nach der IP addresse suchen, die das Gegenüber hat, um bei statischen IP addressen auf andere Geräte sich verbinden zu können.

PC -> unbekanntes-Gerät

Quelle:
https://superuser.com/questions/399550/how-to-talk-to-device-with-fixed-ip-address
