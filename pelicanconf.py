#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Julian'
SITENAME = 'Future self: find this content'
SITEURL = 'https://j2639.gitlab.io'

PATH = 'content'

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
DISPLAY_PAGES_ON_MENU = True

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('jvns', 'https://jvns.ca/'),
         ('MD cheatsheet', 'https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet'),
         )

# Social widget
SOCIAL = (
    ('Mastodon', 'https://chaos.social/@smartsammler'),
    ('Github', 'https://github.com/smartsammler'),
    ('Tumbler', 'https://ver-rissen.tumblr.com/'),
    ('WordPress', 'https://smartsammler.wordpress.com/'),
    )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
