Title: pandoc's math mode and spaces
Author: Julian
Date: 2018-01-29
Modified: 2018-01-29
Category: LaTeX
Tags: latex, pandoc, math-mode, markdown, space, in-a-nutshell
Summary: Don't use digits after opening and spaces before closing dollar signs in markdown
Lang: en
Status: published

Pandoc math behaviour
=====================

If one uses pandoc to generate \LaTeX{} code from markdown it behaves strange when using mathmode and spaces before closing dollar sign.

$x_1+1=2$ written as ```$x_1+1=2$``` works, but it does not if you have an additional space ```$x_1+1=2 $```.
 Then it tries to render it like $x_1+1=2 $ and \TeX{} may fail because of missing/unmatched dollars and math formula outside of math mode.


Minimal working example
=======================

Use this markdown file named ``pandoc.md`` as input for pandoc and generate a \TeX{} file from it with the following line:
```
pandoc -t latex -f markdown -o pandoc.tex pandoc.md
```

Afterwards compile the \LaTeX{} file ``pandoc_main.tex`` which contents
```
\documentclass{article}
\usepackage{hyperref}
\begin{document}
\include{pandoc}
\end{document}
``` 

and compile it with e. g. 
```
pdflatex pandoc_main.tex
```

Feature
=======

This behaviour is desicribed in the man-page for pandoc in the paragraph `Extension: tex_math_dollars`. 
There it says that the opening dollar must be following a space and the closing dollar must not be following a space and must not been followed by a digit.
This prevents one from writing in math mode if one wants to write about economy, lets say about $1,000,000.

Unfortunately ``` \( \) ``` do not work, though pandoc the produced \LaTeX{} code will use this newer style.
So \( x_1+1=2 \) does not render as expected.

[This as a rendered PDF](pandoc_main.pdf)
