Title: Bilder mit Punkt und Pfad-Include
Author: Julian
Date: 2017-02-24
Modified: 2017-02-28
Category: LaTeX
Tags: latex, image, includegraphics, path, graphic, in-a-nutshell
Summary: curly braces
Lang: de
Status: published


LaTeX Bilder mit Punkt oder Unterstrich
=======================================

Wenn man in LaTeX in Bildern Sonderzeichen wie den Punkt oder Unterstriche hat, die normalerweise von TeX entweder als Mathezeichen interpretiert werden oder als Trennzeichen für die Dateiendung, muss man diese die Dateien umbenennen oder sich mit folgender Syntax aushelfen:
Den Dateinamen in geschweifte Klammern setzten, also
```LaTeX
\includegraphics{{plot_a0.4_b0.6}.pdf}
```
Gefunden auf [tex.sx](https://tex.stackexchange.com/questions/10574/includegraphics-dots-in-filename)


Bilderpfad
==========

Oft hat man seine Bilder an verschiedenen Orten, beispielsweise in der Nähe der Auswerteskripte oder in einem zentralen Ordner für Bilder oder in alten Präsentationen und Veröffentlichungen. Damit man nicht immer den absoluten oder relativen Pfad bei jedem Bild neu einfügen muss, empfiehlt es sich die entsprechenden Orte zu Beginn der LaTeX-Datei mit dem `graphicspath` die Pfade global hinzuzufügen. Von diesen werden dann die Bilder gesucht. Das geht auch mit mehreren Pfaden, die jeweils in geschweifte Klammern gesetzt werden. Im folgenden Beispiel wird in den Ordnern `img` und `latex/img` und natürlich im Pfad der TeX-Datei gesucht

```LaTeX
\graphicspath{{/home/username/latex/img/}{img/}}
```
Dafür muss das Paket `graphicx` mit 
``` LaTeX
\usepackage{graphicx}
```
eingebunden sein.

**Achtung**: Die Pfade müssen auf `/` enden.
Siehe auch [tex.sx](https://tex.stackexchange.com/questions/139401/how-to-use-graphicspath).
