Title: container
Author: Julian
Date: 2018-11-27
Modified: 2018-11-27
Tags:
Summary:
Lang: en
Status: draft

Docker is the standard for this, but there are also alternatives that aim at scientists, data scientists and non-root users.
Namely [sylabs'](https://sylabs.io) [singularity](https://github.com/sylabs/singularity) and [red hat's podman](https://github.com/containers/libpod).
Podman supports Dockerfiles and their own standard and do not need to have root privileges. 
Singularity focusses more on scientific needs and HPC.

