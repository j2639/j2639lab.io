Title: Via GRUB von USB booten
Author: Julian
Date: 2017-10-05
Modified: 2017-10-05
Tags: grub, boot, usb, bios
Summary:
Lang: de
Status: draft

Artikel zu [via GRUB 2 von USB booten](http://www.linuxpresso.de/via-grub-2-von-usb-booten/).
Beispielsweise, wenn das BIOS nicht von USB booten kann oder gar nicht erst den USB-Stick anzeigt, kann dieses Verfahren trotzdem funktionieren.

```
set root='(hd1,1)'
chainloader +1
boot
```

