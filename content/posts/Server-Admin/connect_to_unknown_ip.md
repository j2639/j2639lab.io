Title: Connect to unknown IP/host
Author: Julian
Date: 2017-04-06
Modified: 2017-04-06
Tags: ipv6, headless, rfc, podcast, in-a-nutshell, links, ssh
Summary: ping6 ff02::1%eth0
Lang: de
Status: published

Um auf ein IPv6 fähiges Gerät zu kommen, dessen IP man nicht mehr kennt, aber dessen SSH-Passwort man noch kennt, kann man IPv6s Multicast-Fähigkeit nutzen.

Man verbindet seinen Rechner mit dem anderen Gerät direkt via LAN.
Mit `ip mon link` oder `ifconfig` kann man herausfinden, wie das Interface heißt, an dem man das andere Gerät angeschlossen hat.
Mit diesem wissen können wir auf dem Interface (sei hier mal `eth0`) ein Multicast-Ping schicken
``` bash
ping6 ff02::1%eth0
```
. 
Das sollte einerseits die eigene lokale IP zurückliefern, andererseits die fremde.
Auf diese fremde IPv6-Adresse kann man dann per SSH verbinden, wobei man auch hier das Interface angeben muss, da es lediglich eine Link-Local-Adresse ist
``` bash
ssh fe80:...%eth0
```
.

Das ganze habe ich aus dem großartigen Podcast [RequestForComments](https://requestforcomments.de/archives/351).
