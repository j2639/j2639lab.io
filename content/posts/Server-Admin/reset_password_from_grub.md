Title: reset_password_from_grub
Author: Julian
Date: 2018-03-02
Modified: 2018-03-02
Tags:
Summary:
Lang: en
Status: draft

At the grub2 screen press `e` and add a `init=/bin/bash` at the end of your kernel line.

Remount the devices in rw mode:
mount -o remount,rw /
mount -o remount,rw /proc
passwd
sync
systemctl -f -f reboot



LVM
===
When you are using LVM and not plain partitions, you need to proceed a little bit different.

(Boot into a live system)
`lvm2` should be installed.

If necessary, load the necessary kernel module:
```
$ sudo modprobe dm-mod
```

Scan your system for LVM volumes and identify in the output the volume group name that has your Fedora volume:
```
$ sudo vgscan
```

Activate the volume (here `fedora`):
```
$ sudo vgchange -ay fedora
```

Find the logical volume that has your Fedora root filesystem:
```
$ sudo lvs
```

Create a mount point for that volume and mount it:
```
$ sudo mkdir /mnt/tmp
$ sudo mount /dev/fedora/root /mnt/tmp -o rw,user
```

Now you can chroot into the directory and change the password
```
# chroot /mnt/tmp
# passwd
```
