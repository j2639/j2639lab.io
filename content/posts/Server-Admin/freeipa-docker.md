Title: Links about FreeIPA in Docker
Author: Julian
Date: 2017-03-10
Modified: 2017-03-10
Tags: freeipa, ipa, docker, links, todo
Lang: en
Status: published

# Links to resources for test deployment of FreeIPA into docker

In the [official page](http://www.freeipa.org/page/Docker) you can find three useful links to the Docker container, the github repo and, also useful for other things, the documentation on how to dockerize complex software (like FreeIPA).

Another interesting reading is the missing documentation of the [FreeIPA API](https://vda.li/en/posts/2015/05/28/talking-to-freeipa-api-with-sessions/)


# IPA troubleshooting
[Backup](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/config-files-logs#conf-file-ref)
[Replicas](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/creating-replicas-old#replica-install-without-dns-old)
[Recovering from lost CA Server](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Linux_Domain_Identity_Authentication_and_Policy_Guide/trouble-replica.html#trouble-replica-ca-recovery)
[Replica with special CA](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/creating-replicas-old#replica-install-ca-old)
[Promoting CA Server](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Linux_Domain_Identity_Authentication_and_Policy_Guide/server-roles.html#server-roles-promote-to-ca)
[Installing IPA Server](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Linux_Domain_Identity_Authentication_and_Policy_Guide/install-server.html#install-server-without-ca)
[Replica](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/linux_domain_identity_authentication_and_policy_guide/creating-the-replica#replica-install-setup-ca)
