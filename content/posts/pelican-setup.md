Title: Blog mit Pelican und git
Date: 2017-02-25 11:11
Modified: 2017-09-05 11:50
Category: Misc
Tags: pelican, publishing, git, todo
Slug: pelican-blog-mit-git-deployment
Summary: Blog mit Python Pelican und Git deployment
Status: published

Einrichten des Basissystems
===========================
Dies wird mein neuestes Blog, welches nicht ein selbst oder fremdgehostetes Wordpress ist, sondern ein mit [Pelican](https://getpelican.org) statisch generiertes Blog, dass dann mit [Git](https://git-scm.org) auf meinen Server deployt wird, wie es so ähnlich in [anderes Blogs beschrieben](http://nicolasgallagher.com/simple-git-deployment-strategy-for-static-sites/) wird.
Das hat den Vorteil, dass ich auf dem Server eine Software weniger laufen lassen muss und wahrscheinlich auch recht leicht auf [GitHub](https://github.com) oder [GitLab](https://gitlab.com) deployen könnte.
Außerdem muss man das Makefile anpassen, sodass es bei `make clean` nicht das Verzeichnis `output` löscht, sondern lediglich dessen nicht versteckten Inhalt, da sonst jedes mal das Git-Repository gelöscht wird.

Natürlich muss man darauf achten, dass die Rechte, User- und Gruppenzugehörigkeit des Hooks (`~/blog_html.git/hooks/post-receive`) und des Zielverzeichnisses (`/var/www/blog/`) stimmen und dass man nur erlaubte Kommandos verwendet, wenn man selbige für bestimmte Benutzer (git) eingeschränkt hat.

Themes
------
Die Themes sind in einem eigenen [Git-Repository](https://github.com/getpelican/pelican-themes.git), das man auch noch klonen muss, damit das Setup funktioniert.
Entweder inklusive aller submodule
```
git clone --recursive https://github.com/getpelican/pelican-themes themes
```
oder, wenn man bereits weiß, welches man haben möchte, nur dieses
```
git clone https://github.com/getpelican/pelican-themes themes
cd themes
git submodule update --init plumage
```

Bei Plugins verhält es sich ebenso.
<!-- Add Jupyter notebook to pelican incl. its how-to, read the Fedora-Magazine article on pelican, git and gitlab -->

Jupyter-notebook-Plugin
-----------------------
Mit Hilfe des [Pelican-ipynb-Plugin](https://github.com/danielfrg/pelican-ipynb) ist es mir möglich auch direkt aus [Jupyter notebooks](https://jupyter.org) heraus zu bloggen. Da ich ein großer Fan von Jupyter notebooks bin und es sich anbietet in diesen seinen Quelltexte zu dokumentieren, ist es eine sehr gute möglichkeit seine Python-lastigen Artikel in diesen zu entwickeln.
Die installation des Plugins ist sehr einfach und auch das Bloggen besteht im wesentlichen aus einer Kopie des Notebooks und erstellen einer Metadaten-Datei.

```
cd plugins
git submodule update --init ipynb
```
