Title: Strange behaviour when using VPN
Author: Julian Gethmann
Category: Unixoid
Tags: vpn, openvpn, route, routing, ip, problem, question, ssh
Summary:
Lang: en
Status: draft

Host A (129.1.1.10) runs openVPN within an institute's domain
Host B (129.1.1.20) runs no openVPN, but is in the same network as Host A

Host A gets its VPN-IP-address (141.1.1.10) (for tap0) via DHCP as well as a new routing table.
Host B can't connect to Host A anymore, since the new routing table includes an unroutable route for the subnet Host A and B are in with their physical devices.

If Host A deletes the new strange route
``` bash
sudo ip del route del 129.1.0.0/16 via 141.1.1.254 dev tap0
```
Host B can connect Host A again and Host A can still connect Host B in the terminal which killed the route.

*But* Host A cannot connect via SSH to Host B using its IP (129.1.1.20), though it can using its hostname (which is in the `/etc/hosts` file. In addition Host A can also ping Host B both using its IP and its hostname.

After a while one can connect via the IP inside of a newly created shell.

So I guess some wired route caching is going on in connection with DNS stuff.


Helpful commands to get this far
================================

List connected SSH clients with their IP: 
```
sudo netstat -tnpa | grep 'ESTABLISHED.*sshd'
```

and of course `ssh -vvvv`, `ping`, `traceroute` `ip route show` and `ifconfig`

Newes developement (2017-03-09)
-------------------------------
* if B is inside anka-vpn and wants to connect to anka-vpned A: ping sends (DUP) packages
* if A is in in ANKA-VPN and B is KIT-VPN B, but not in 129.13.108.0/16 B cannot connect to A via ssh

Helpful resources
=================

[don't get routes](https://askubuntu.com/questions/788803/how-to-change-the-metric-of-an-openvpn-client-default-route)
[run script after connection](https://askubuntu.com/questions/28733/how-do-i-run-a-script-after-openvpn-has-connected-successfully)
