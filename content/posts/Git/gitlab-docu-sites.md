Title: gitlab-docu-sites
Author: Julian
Date: 2017-09-05
Modified: 2017-09-05
Tags: readthedocs, python, gitlab, sites, gitlab-sites, docu
Summary:
Lang: en
Status: draft

How to use GitLab sites to provide your users docu without using [readthedocs](https://readthedocs.org)?

* GitLab-CI for building the documentation
* Hook? for pushing it to the gl-sites branch?
* Link to gl-sites branch?

Link-Tipp: Pelican site with GitLab
