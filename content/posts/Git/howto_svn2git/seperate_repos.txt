Perhaps one could split the SVN repo into these GIT repos

Projekte:
Jena.git
Texvorlagen → JenaMessaufbau.git
SCU_Feldfehler_Shimming.git
SR-Spektroskopie.git
Opera_generische_Modelle.git

texmf.git
texvorlagen.git (Ausschreibung, …)

utilities:
elegant.git
misc.git
opera.git
spec.git
spectra.git
srw.git

(currently exists on las101:/home/repos/systemdocu.git docu and small scripts and modified config files where the binaries (PDFs, etc.) are stored under /las-archiv1/system e. g. UpdateFedora.tex; perhaps move to /las-archiv1/system as well)
