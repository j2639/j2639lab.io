Title: github-workflow
Author: Julian
Date: 2017-07-26
Modified: 2017-07-26
Tags: links
Summary:
Lang: en
Status: draft

GitHub workflow
https://gist.github.com/Chaser324/ce0505fbed06b947d962

Applying and creating patches (to fix vim-bundle plugins)
https://www.devroom.io/2009/10/26/how-to-create-and-apply-a-patch-with-git/

GitLab workflow

Good commit messages:
https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#commit-messages-guidelines
https://chris.beams.io/posts/git-commit/#seven-rules
