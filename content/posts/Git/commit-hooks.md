Title: Commit hooks
Author: Julian
Date: 2017-03-08
Tags: Git, hooks, Python, todo
Summary: How working with Git hooks can be a pleasure
Lang: en
status: published

How to organize Git hooks
=========================

[Git commit hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) are a very pleasant invention though relative few people know about them except from the server side components at Github or GitLab.

But beside the server side hooks Git has also client side hooks. So if want to have quality assurance for not public open source projects you host on Github and where you can use e.g. Travis-CI and your IT's Jenkins does not provide workflows that fit you, you can use local hooks.

The drawback is that the hooks are not part of the repository and therefore need to be in a separate repository.

The little Python tool [pre-commit](http://pre-commit.com/) which can be installed via `pip` is intended to simplify the process.

You can create a `.pre-commit-hooks.yaml` file which lists the hooks to use. This file can be part of the repository and therefore can be used to setup the repo on other machines without caring about extra repos for your individual hooks and manually copying them.
By running `pre-commit` you create and install your hooks which will then be run at each interaction locally.

Especially for Python there are many useful hooks already available, and since it is integrated with PyScaffold it is even easier to use it.

<!-- Add usage demo and scripting demo -->

<!-- Create hook for shell scripts using shellcheck -->
