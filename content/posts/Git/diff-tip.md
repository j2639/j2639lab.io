Title: Diff tip
Author: Julian
Date: 2018-04-26
Modified: 2018-04-26
Tags: git, diff, in-a-nutshell, versions
Summary: Diff two files sign by sign
Lang: en
Status: draft

Find e. g. deleted spaces or version number changes in `requirements.txt`. 

```
git diff --word-diff-regex=. fileA fileB
```
Instead of `fileA` and `fileB` sha hashes of the commits are also usable.

Thanks to [@robinro](https://twitter.com/robinro) for this tip.
