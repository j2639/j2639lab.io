Title: signed tag with S/MIME cert
Author: Julian
Date: 2021-09-02
Modified: 2021-09-02
Tags: smime, git, sign
Summary: Sign a tag or commit with an S/MIME certificate
Lang: en
Status: draft

Make your system aware of your S/MIME key.
For this you need to find out which keys are available already:
```
gpgsm --list-secret-keys
```
Here you can find the ID.
If not, then your certificate is not installed, yet. This can be done also with gpgsm:
```
gpgsm --import ${path_to_your_PKS12_file.p12}
```

Now you can export your signkey into the variable `${signingkey}`, so that you can set it in your git config
```
echo "export signingkey=<...>" >> ~/.profile && source ~/.profile
```

First you have to configure your git to use S/MIME x509 certificates instead of the default GPG certificates for signing:
```
git config --global user.signingkey ${signingkey}
git config --global gpg.format x509
```

Then you can actually sign your new tag `${TAGNAME}` and provide a description for it `${TAG_DESCRIPTION}`.
```
git tag -s ${TAGNAME} -m "${TAG_DESCRIPTION}"
```


With 
```
git tag --verify ${TAGNAME}
```
you can check that the signature is correct.
