Title: git-server
Author: Julian
Date: 2017-08-07
Modified: 2017-08-07
Tags:git, server, init
Summary:
Lang: de
Status: draft

Server sicher installieren
==========================

```
git init --bare
```

User und Server installieren
============================
```
sudo groupadd --system git
# sudo useradd --shell $(which git-shell) --comment 'git version control' git --gid git --base-dir /las-archiv1/ --home-dir /las-archiv1/repos --create-home
useradd --shell $(which git-shell) --comment 'git version control' git --system --home-dir /las-archiv1/git
# sudo usermod -L git
passwd git
mkdir -p /las-archiv1/git/.ssh
touch /las-archiv1/git/.ssh/authorized_keys
chmod 600 /las-archiv1/git/.ssh/authorized_keys
chmod 700 /las-archiv1/git/.ssh
cp -r /usr/share/doc/git/contrib/git-shell-commands /las-archiv1/git
chmod 750 /las-archiv1/git/git-shell-commands/*
chown -R git:git /las-archiv1/git/
```
usermod -p oed1ruuZahB2uchohsieJei1Caphie git
usermod --unlock git
chmod +x /las-archiv1  # to allow git to login


GitLab mirror
=============
First you bare-clone the repository from the GitLab instance and give it the `git` ownership.

```
git clone --bare https://git.scc.kit.edu/las/ansible.git /las-archiv1/git/ansible.git 
chown -R git:git /las-archiv1/git/ansible.git
```
If the repository is not big you can also try the approach to create a bare repository as shown below.

And then you setup the GitLab instance to remote push at every push. You go to Settings -> Repository -> Push to remote repository (as described in the [offical docu](https://docs.gitlab.com/ee/workflow/repository_mirroring.html#pushing-to-a-remote-repository))
and provide the new URL.


Dem git-Benutzer erlauben neue Repositories anzulegen
=====================================================

Via [stackoverflow](https://stackoverflow.com/a/28739802)


For example create ~git/git-shell-commands/newgit with this content:
```
#!/bin/sh
mkdir -p ~/$(basename "$1" .git).git
cd ~/$(basename "$1" .git).git
git init --bare
```

Then `chmod +x ~git/git-shell-commands/newgit` to allow execution.

To use it, run:
```
ssh git@server newgit newrepo.git
```
this is enough to create a new bare git repository, it can be cloned using:
```
git clone git@server:newrepo.git
```
