Title: Delete all already deleted files
Author: Julian
Date: 2017-08-06
Modified: 2017-08-06
Tags: in-a-nutshell
Summary: Delete all files you previously deleted in the filesystem
Lang: en
Status: draft

Add all tracked files and git-rm all tracked files which are not in the filesystem any more.
``` bash
git add -u
```

To just delete the files that are not in the filesystem, but not also add all changed files run
``` bash
git ls-files --deleted -z | xargs -0 git rm 
```
