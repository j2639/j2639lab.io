Title: gitlab-runner
Author: Julian
Date: 2017-06-21
Modified: 2017-06-21
Tags: gitlab, runner
Summary: Installation of GitLab runner on Fedora systems (<31)
Lang: en
Status: draft

Installing and configuring worker and CI in Docker container
==============================================================
### https://docs.gitlab.com/runner/executors/docker.html
Run as root
```
dnf install docker docker-selinux # docker-latest-zsh-completion.x86_64

systemctl start docker
```

and create the config directory `mkdir -p /srv/gitlab-runner/config/`

### https://docs.gitlab.com/runner/install/docker.html
```
docker run -d --name gitlab-runner --restart always \
  --privileged  # needed for f30 and current 2019-11 gitlab-runner
  -v /var/run/docker.sock:/var/run/docker.sock:Z \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner:Z \
  gitlab/gitlab-runner:latest
```

```
docker exec -it gitlab-runner gitlab-runner register
```

#### https://docs.gitlab.com/ce/ci/runners/README.html
Im webinterface unter CI-> runners den token kopieren
```
https://git.scc.kit.edu/ci
py_cassandra
python
Registering runner... succeeded                     runner=ycRSTYmp
docker
python:onbuild

mkdir -p /srv/gitlab-runner/config
```

### https://github.com/dpw/selinux-dockersock
```
dnf install policycoreutils policycoreutils-python checkpolicy
git clone https://github.com/dpw/selinux-dockersock.git
cd selinux-dockersock/ && make && semodule -i dockersock.pp
# semodule -r dockersock # to remove the SELinux modul
```


Troubleshooting with Docker
===========================

Problem-Beschreibung https://stackoverflow.com/questions/36918387/space-issue-on-docker-devmapper-and-centos7

Lösung: https://www.projectatomic.io/blog/2015/06/notes-on-fedora-centos-and-docker-storage-drivers/
# systemctl stop docker
# rm -rf /var/lib/docker

/etc/sysconfig/docker-storage to include the line:
DOCKER_STORAGE_OPTIONS= -s overlay

# systemctl start docker


No space
--------
las101

Not working after / switch.
addgroup docker
dnf install containerd


Permission denied when runner wants to delete cached files in image
-------------------------------------------------------------------
Set `disable_cache = true` in the `/srv/gitlab-runner/config/config.toml` and restart the container with
`docker stop gitlab-runner; docker ps` get the image hash: `docker rmi 123456789; docker restart gitlab-runner`.
Probably `docker restart gitlab-runner` will also work. Maybe this also breaks after rebooting

Installing worker on Fedora 25 (outdated)
=========================================
[Gitlab-CI-multi-runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner)
/srv/gitlab-runner/config.toml ist der Ort statt /etc/gitlab-runner/config.toml unter Fedora
https://docs.gitlab.com/ee/ci/docker/using_docker_images.html
https://docs.gitlab.com/ee/ci/


install `gitlab-ci-multi-runner` from their repos (see my ansible role)


run the following docker command

```
sudo gitlab-ci-multi-runner register \
  --url "https://git.scc.kit.edu/ci" \
  --registration-token "dGx49XfxZxcHR8WaSEcN" \
  --description "docker-python-las113" \
  --executor "docker" \
  --docker-image "python:3.5"
```

