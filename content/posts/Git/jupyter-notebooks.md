Title: Jupyter notebooks and Git
Author: Julian
Date: 2018-07-05
Modified: 2018-07-05
Tags: Python, Jupyter, notebook, Diff
Summary:
Lang: en
Status: draft

How to handle Jupyter notebooks with Git?
=========================================

Use [nbdime]() or even better use [jupytext](https://github.com/mwouts/jupytext/).
Also have a look at my meetup presentation about Jupyter and Joel Grus' rant about notebooks (I don't like notebooks)
