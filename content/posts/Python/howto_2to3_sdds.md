``` bash
ln -s sddsdatamodule.so sddsdata.so
```

``` bash
locate Python.h

for file in `cat <<-EOL
Python.h
patchlevel.h
pyconfig.h
pyconfig-64.h
pymacconfig.h
pyport.h
pymacro.h
pyatomic.h
dynamic_annotations.h
pymath.h
pytime.h
object.h
pymem.h
objimpl.h
typeslots.h
pyhash.h
pydebug.h
bytearrayobject.h
EOL`; do 
  cp /usr/include/python3.4m/${file} .
done

cp /home/gethmann/miniconda3/include/lzma.h .
cp -r /home/gethmann/miniconda3/include/lzma . 
```

Im Makefile manuell eintragen:
PYTHON_INCLUDE = /usr/include/python3.4m/

