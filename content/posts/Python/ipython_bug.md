Title: ipython_bug
Author: Julian
Date: 2017-07-27
Modified: 2017-07-27
Tags: bugs, ipython, jupyter, str
Summary: Strange behaviour when using IPythons `?`
Lang: en
Status: draft

Text I wrote when I wanted to comment on Bug/Feature https://github.com/ipython/ipython/issues/10226
But why is it triggered when I use `>>> "".replace?` and not when I use `>>> "".format?`


Then I noticed that in Jupyter notebook both trigger a new cell.

Terminal:
=========
```
In [1] "".replace?
Object `replace` not found.

In [1] "".format?
Sign....
```

Notebook:
---------
```
In [1] "".replace?
Object `replace` not found.
```
(no help)

```
In [2] "".format?
Sign...
```

-------------------
```
In [1] "".format?
Sign...
```

```
In [2] "".replace?
Object `replace` not found.
```

(not reproducable, probably a different bug (e. g. in the JavaScript)


Issue with other functions
--------------------------
```
In [1] "".split?
Object `split` not found
```
but different Help texts for str.format? vs. "".format?:

```
In [6]: str.format?
Docstring:
S.format(*args, **kwargs) -> str

Return a formatted version of S, using substitutions from args and kwargs.
The substitutions are identified by braces ('{' and '}').
Type:      method_descriptor

In [7]: "".format?
Signature: format(value, format_spec='', /)
Docstring:
Return value.__format__(format_spec)

format_spec defaults to the empty string
Type:      builtin_function_or_method
```

