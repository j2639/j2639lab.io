# coding=utf-8
"""Handle all issues concerning time and dates"""

import datetime

import pytz
from pytz import utc

berlin = pytz.timezone('Europe/Berlin')


def naive_to_berlin(dt: datetime.datetime) -> datetime.datetime:
    pass


def naive_berlin_to_utc(dt: datetime.datetime) -> datetime.datetime:
    return aware_berlin_to_utc(naive_as_berlin(dt))


def naive_as_berlin(dt: datetime.datetime) -> datetime.datetime:
    """

    Args:
        dt:

    Returns:


    Examples:
        >>> naive_to_berlin(datetime.datetime(2018,11,11,11,11))
        datetime.datetime(2018, 11, 11, 11, 11, tzinfo=<DstTzInfo 'Europe/Berlin' CET+1:00:00 STD>
    """
    return berlin.normalize(berlin.localize(dt))


def naive_as_utc(dt: datetime.datetime) -> datetime.datetime:
    """
    .. seealso:: :func:`cassandra.tz.naive_as_berlin`
    """
    return utc.normalize(utc.localize(dt))


def aware_rename_tz(dt: datetime.datetime, tz: str = "Europe/Berlin") -> datetime.datetime:
    """Rename the timezone, but do not shift it accordingly."""
    return dt.replace(tzinfo=pytz.timezone(tz))


def aware_utc_to_berlin(dt: datetime.datetime) -> datetime.datetime:
    return dt.astimezone(berlin)


def aware_berlin_to_utc(dt: datetime.datetime) -> datetime.datetime:
    #return utc.normalize(dt.astimezone(utc))
    return dt.astimezone(utc)


def aware_to_naive_utc(dt: datetime.datetime) -> datetime.datetime:
    if dt.tzinfo is None:
        raise ValueError('dt must be timezone aware!')
    dt = aware_berlin_to_utc(dt)
    return aware_as_naive(dt)


def aware_to_naive_berlin(dt: datetime.datetime) -> datetime.datetime:
    if dt.tzinfo is None:
        raise ValueError('dt must be timezone aware!')
    dt = aware_utc_to_berlin(dt)
    return aware_as_naive(dt)


def aware_as_naive(dt: datetime.datetime) -> datetime.datetime:
    return dt.replace(tzinfo=None)


def nsepoch2utc(time: int) -> datetime.datetime:
    return datetime.datetime.utcfromtimestamp(time // 1e9)
