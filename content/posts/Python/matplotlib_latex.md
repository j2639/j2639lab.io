Title: Enhance 	matplotlib and LaTeX
Author: Julian
Date: 2017-03-22
Modified: 2017-03-22
Tags: latex, matplotlib, python
Lang: en
Status: published


Plotting for LaTeX documents with matplotlib 
============================================

To use $\LaTeX$ efficiently you should use [raw-strings]() (`r"$\LaTeX^2$"`) otherwise you need to escape the backslashes with backslashes.
To use $\LaTeX$ and [format-strings]() you need to escape all curly braces with curly braces, e. g. `r"{} $\textrm{{nah}}$".format("escaping")`.

Standard official documentation. Not that interesting for intermediate matplotlib users:
[TeX rendering with LaTeX](http://matplotlib.org/users/usetex.html)

But the [PGF](http://matplotlib.org/users/pgf.html) tutorial is great since it shows you how to realy benefit from $\LaTeX$ inside matplotlib.

Use PGF also for PDF rendering:
``` Python
from matplotlib.backends.backend_pgf import FigureCanvasPgf
matplotlib.backend_bases.register_backend('pdf', FigureCanvasPgf)
```
and use your custom Preamble (which can also be but into your [matplotlibrc]():
``` Python
matplotlib.rcParams.update(
    {"pgf.preamble": [
        r"\usepackage{siunitx}",
	r"\usepackage{upgreek}",
    ]
)
```
and if your using XeLaTeX (`"pgf.texsystem": "xelatex",`) you can also go further.

