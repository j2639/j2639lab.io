// gcc -shared -fPIC -o Bylib.so By.c
#include "math.h"

double By(int n, double args[n]){
	// z=0, _=1, kx=2, x=3, ky=4, y=5, kz=6, Bw=7
	double Bw = args[7]; // wiggler field (T)
	double kx = args[2];  
	double kz = args[6];
	double ky = args[4];
	double x = args[3];
	double y = args[5];

	return Bw * cos(kx * x) * sin(kz * args[0]) * cosh(ky * y) ;
}
