// gcc -shared -fPIC -o fullBy.so fullBy.c
// import ctypes
// lib = ctypes.CDLL('/tmp/fullBy.so')
// lib.fullBy.restype = ctypes.c_double
// lib.fullBy.argtypes = (ctypes.c_int, ctypes.c_double)
// lib.fullBy()
#include "math.h"

double fullBy(const double x, const double y, const double z, const double kx, const double kz, const double Bw)
{
	const double ky = sqrt(kz - kx) * sqrt(kz + kx);

	return Bw * cos(kx * x) * sin(kz * z) * cosh(ky * y) ; 
}
