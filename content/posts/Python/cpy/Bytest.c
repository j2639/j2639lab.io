// gcc -lm Bytest.c
#include "math.h"
#include <stdio.h>

int main() {
	const double Bw = 2.9; // wiggler field (T)
	const double kz = 2 * M_PI / 0.0514;

	const double kx = 22; 
	const double x = 1e-3;
	const double y = 0.0; // 5.8 is max for kx=10
	const double z = 7.1; // args[0]

	const double a = sqrt(kz*kz - kx*kx);
	const double b = sqrt(kz + kx) * sqrt(kz-kx);
	printf("%.18g %.18g", a, b);
	printf("\na==b? %d", a == b);

	puts("\n");

	const double ky = sqrt(kz+kx) * sqrt(kz-kx);  // should be numerically more robust than sqrt(kz**2 - kx**2)

	printf("By(kx=%.2g, ky=%.2g, x=%.1g, y=%.1g, z=%.1g) = %.20g", kx, ky, x, y, z,
			Bw * cos(kx * x) * cosh(ky * y) * sin(kz * z));
	printf("\ncos(kx * x)=%.20g", cos(kx * x));
	printf("\nsin(kz * z)=%.20g", sin(kz * z));
	printf("\ncosh(ky * y)=%.20g", cosh(ky * y));
}
