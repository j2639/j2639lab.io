Title: numpy_diag
Author: Julian
Date: 2017-07-24
Modified: 2017-07-24
Tags: numpy, np, diagonal, diag, matrix, sparse, cov, covariance, multi
Summary: 
Lang: en
Status: draft

Often one needs to have a (sparse) diagonal matrix with ones or other values.
e. g. for the covariance matrix for `np.random.multivariate_normal` one needs such one for examples.

<!--`np.ones` can generate a matrix full of ones and `np.zeros` one with zeros.-->

We use `np.diag` for this ;)
```
>>> np.diag([1,2,3])
array([[1, 0, 0],
       [0, 2, 0],
       [0, 0, 3]])
```
