Title: SQL learning resources
Author: Julian
Date: 2023-11-18
Category: Python
Tags: SQL, Databases, links, pandas, polars, python, postgres, duckdb, DataFrame
Summary: Learning resources for SQL
Lang: en
Status: published

SQL introductions
=================
* [SQL for data analysis](https://hakibenita.com/sql-for-data-analysis) is an introduction to SQL for Python data analysts. It compares SQL's capabilities with Pandas' and shows how and when to use which of these two technologies.
* From the book named “SQL for data scientists” there is an [editor](https://sqlfordatascientists.com/sql-editor-test/) to test your SQL queries online.

Less serious, but more funny approaches to learning SQL are the three gamified SQL resources
* [Interactive beginner friendly tutorial](https://selectstarsql.com/)
* [Text adventure to learn SQL](https://mystery.knightlab.com/walkthrough.html)
* [Game to learn SQL](https://lost-at-sql.therobinlord.com/)o


SQL helpers
============
[JupySQL](https://jupysql.ploomber.io/en/latest/quick-start.html) is SQL in Jupyter notebooks. It includes some plotting functionality and pandas integration. So you can write some SQL to load your data into Pandas DataFrames.
But it also supports DuckDB.

[DuckDB in JupyterLab](https://duckdb.org/?utm_source=ploomber&utm_medium=blog&utm_campaign=jupysql). [DuckDB](https://duckdb.org) itself is a single user one node database for data engineers who do not want to write Pandas or Polars code, but are happy to solve their analysis and data mangling issues with SQL. It is very fast and quite flexible.

Speaking of DuckDB and comparing it with Pandas, polars and alike, there is a [benchmark of database-like ops](https://h2oai.github.io/db-benchmark/) from 2021.

In Julia Evans' [awsome list of playgrounds](https://gist.github.com/jvns/3cfd81a58bcecb11c7f097ebcc8e923d) there are also some SQL playgrounds included of which one is [her own](https://sql-playground.wizardzines.com/).

Hints and tips for SQL
=======================
* PostgeSQL: `# \d my_table` for describing the database scheme
* [Postgres performance analysis](https://www.pgmustard.com/) 

Python specific
===============
[SQLtap](https://github.com/inconshreveable/sqltap)) is a profiler for SQLAlchemy to see where your real bottlenecks are.
[SQLModel](https://sqlmodel.tiangolo.com/) is a library on top of SQLAlchemy and Pydantic abtracting the SQL away from your Python code.

Further readings
================
Also have a look at the [article about dataframes](./dataframes.html).

If you want to have a look at your data inside your terminal, you might be interested in the ?q tools like jq and have a look at [nushell](https://www.nushell.sh/book/loading_data.html) (which I haven't, yet).