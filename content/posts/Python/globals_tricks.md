Title: Tricks with global and local variables
Author: Julian
Date: 2017-03-01
Category: Python
Tags: Python, locals, globals, variables, todo
Summary: Use functions where you have the name as a string and create variables where you also only have the name as a string
Lang: en
Status: published

Fun with locals and globals
===========================

I wanted to know how to use functions and variables where I only get the name as a string.
It isn't like e. g. in PHP that you can put another `$` infront of the variable, since variables do not start with a special sign.
One has to get to know `locals` and the `globals` functions.

<!-- Add more infos about globals (is it a function or what is it?) and its differences between Python 2 and 3 -->

Add variables where you have the name as a string and their initial values.

Since I know of nothing like the PHPs
``` PHP
$foo = 'bar';
$$foo = 3;
echo $bar;
```

One has to write directly into the globals dictionary which is not a function that contains a dictionary (o.O?)
``` Python
>>> foo = 'bar'
>>> globals()[foo] = 3
>>> print(bar)
3
```
. Or another case:
``` Python
for i in ["a", "b", "c"]:
    globals()[i] = 1337
```


Reading
-------
Also if one wants to call a function for which one only has got the name as a string like in PHP
``` php
$foo="foo"; $$foo() 
```

In Python one has to use the `locals` or `globals` functions as far as I know:
``` Python
>>> def foo():
...     print("foo")
>>> 
>>> locals()["foo"]()
"foo"
```
and inside a function:
``` Python
>>> def bar():
...     globals()["foo"]()
```


General remarks
----------------
``` Python
>>> def a():
...     global c
...     c = 3 
>>> a()
>>> print(c)
3
>>> del c
```
