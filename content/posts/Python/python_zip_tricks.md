Title: Tipp zum Python-zip-Modul
Author: Julian
Date: 2023-01-10
Modified: 2024-01-12
Tags: in-a-nutshell
Summary: Tipps and tricks for visualisations in Python
Lang: de
Status: published

Steht auch schon so in der Python Dokumentation zum Sortieren, dass die funktionale Variante die weniger performante (da Kopie nötig) ist, jedoch ist die Bevorzugte

Python short hand for sorting a transversed list
```Python
a = [[1,2,4,3], [5,6,7,8]] → [[1,2,3,4], [5,6,8,7]]
a = zip(*a)
a.sort()  # np.sort(a, axis=0) sorts first each row
a = zip(*a)
or more easy/functional, but less performant: a = zip(*sorted(zip(*a)))
```