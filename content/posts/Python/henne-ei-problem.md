Title: henne-ei-problem
Author: Julian
Date: 2017-05-04
Modified: 2017-05-04
Tags:
Summary:
Lang: en
Status: draft

Wenn man mit Python Pakete entwickelt und diese mit `tox` testen möchte tritt bei mir folgendes Problem auf.

Die Tests importieren das Paket via (`import packagename.module`) oder (`from packagename.module import *`), jedoch ist die dann importierte Version ja noch die zuletzt installierte, d. h. nicht die Version, die noch gar nicht ins git commitet wurde, weil zuvor ja erst die Tests laufen sollen

D. h. entweder laufen die Tests nicht bereits beim Commit, sondern erst vor dem Push oder die Imports gehen geschickter oder es gibt einen Trick, der mir nicht geläufig ist.
Werden bei mit `-e` installierten Paketen auch nicht commitete Änderungen übernehmen? Vllt. funktioniert das auch mit tox.

