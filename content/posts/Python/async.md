Title: async
Author: Julian
Date: 2018-04-02
Modified: 2018-04-02
Tags: async, asyncio, best-practice, good-reads, links
Summary: Good reads and best practices to get into async in Python
Lang: en
Status: draft

Links and good reads about async in Python

Reads
=====

(Trio tutorial)[https://trio.readthedocs.io/en/latest/tutorial.html]
(How the heck does async await work in Python 3.5)[https://snarky.ca/how-the-heck-does-async-await-work-in-python-3-5/]
(A tale of event loops)[https://github.com/AndreLouisCaron/a-tale-of-event-loops]
(Controlling Python Async Creep -- Hackernoon)[https://hackernoon.com/controlling-python-async-creep-ec0a0f4b79ba?source=false---------0]

Code to have Python 2 and 3
===========================

Article
-------
(Python and Async Simplified)[http://www.aeracode.org/2018/02/19/python-async-simplified/]

Best practice code exapmles
---------------------------
(Jinja2 support for Async if applicable)[https://github.com/pallets/jinja/blob/master/jinja2/asyncsupport.py]

Maybe not so best practices are in my cassandra `dev-parallel`-branch aproach and the gitlab downloader. Both are very work-in-progress though.

