Title: Python/windows
Author: Julian
Date: 2021-09-16
Modified: 2021-09-16
Tags: windows, venv, virtualenv, powershell, pyinstaller
Summary: How to set up venvs for usage with Windows' PowerShell and build executables
Lang: en
Status: published

As Python dose not ship with Microsoft Windows 10, you have to install it manually.
Unfortunately, depending on how you do this virtual environments (venv) might work or might not work with the Powershell.
Here I describe the solution for my use case.

Install Python from the installation resource at [Python.org](https://www.python.org/downloads/windows/) for **all** users and with the **PATH** variable set.
Do not use the version provided in the Microsoft store. That did not work for me.

In the PowerShell you probably have to allow the activation script to being executed, by typing:
```
Set-ExecutionPolicy -ExecutionPolicy AllSigned -Scope Process
```
.
That allows this particular PowerShell to run the Activate.ps1 script.
To activate it you must type:
```
.\venv\Scripts\Activate.ps1
```

**Caution**: Now your python executable might not be `python3` or `python` like on Linux and also not `python.exe` like when you install it from the Microsoft store, but it is `py` or `py.exe`!
So do something like `py.exe -m pip install black`.

With `py.exe` you can even select the Python interpreter (version) if you have different installed.
But be aware that `py.exe -3.8` will not select your Python 3.8 venv you activated before, but the main Python 3.8 installation.

PyInstaller
-----------
If you want to be provide your colleagues an exe-File you can do so with PyInstaller.
After installing pyinstaller (`py -3 -m pip install pyinstaller`) you can call it with
```
py -3 -m PyInstaller --onefile my_script.py
```
where my_script.py is the script you want to convert. Mind the case sensitivity!

The exe file will be available in `dist/`.

Troubleshooting
---------------
It might occur that you get Errors like (German version here only):
```
OSError: [WinError 59] Unerwarteter Netwzerkfehler: 'P:\\…'
```
. That might happen if `P:\` happens to be a samba share.
Then moving the script to a local drive (`C:\…`) and running PyInstaller there might work for you, too.
The resulting exe file works on the samba shared drive without complaining about network errors.
