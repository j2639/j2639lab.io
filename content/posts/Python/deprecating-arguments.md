Title: deprecating-arguments
Author: Julian
Date: 2019-02-21
Modified: 2019-02-21
Tags: deprecation, arguments
Summary:
Lang: en
Status: draft

How to deprecate arguments properly?
====================================

One should use `warings.warn("Text", DeprecationWarning)` instead of raising an exception, as stated in an [stackoverflow answer](https://stackoverflow.com/questions/29378505/how-do-i-raise-deprecationwarnings-in-python-2-7)

But if I have a signature
```
func_name(argument, b)
```
and I want to have 
```
func_name(a, b)
```
instead, what do I have to do?

``` Python
def func_name(a=None, b=None, *, argument=None):
    if argument:
	    warnings.warn("Using `argument` is deprecated since version 1. Use `a` instead.", DeprecationWarning)
	if not b and not (a or argument):
	    raise ValueError("You must provide `a` and `b`.")
```
That works at first sight, because it preserves the position of the intended arguments, but it comes with the drawback of making all positional arguments keyword arguments.
That might cause someone to interpret them as such, which is not intended!


Pre-Deprecation
---------------
There should be three steps.
0. FutureWarning if this feature is not stable, yet.
1. PendingDeprecationWarning when you are thinking about the deprecation.
2. DeprecationWarning with the date or release when the feature will be disabled.
3. Disabeling the feature.


Stacklevel
----------
As @asottile pointed out in a youtube video, you almost always want to use `stacklevel=2` as an kwarg for the deprecation warning.
It shows the code that uses the deprecated function instead of the deprecated function.

Decorator solution
-------------------
The following solution I found in [another discussion](https://stackoverflow.com/questions/49802412/how-to-implement-deprecation-in-python-with-argument-alias)  
works also just for keyword arguments, but does not work for positional arguments.
``` Python
def deprecated_alias(**aliases):
    def deco(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            rename_kwargs(f.__name__, kwargs, aliases)
            return f(*args, **kwargs)
        return wrapper
    return deco

def rename_kwargs(func_name, kwargs, aliases):
    for alias, new in aliases.items():
        if alias in kwargs:
            if new in kwargs:
                raise TypeError('{} received both {} and {}'.format(
                    func_name, alias, new))
            warnings.warn('{} is deprecated; use {}'.format(alias, new),
                          DeprecationWarning)
            kwargs[new] = kwargs.pop(alias)

```
