Title: Compare functions for sort and sorted
Tags: Python, compare, function, key, sort, sorted
Lang: en 

# Compare functions for sort and sorted

If one wants to write a function that can act as the `key` for `sort` and `sorting` it only needs to return negative, positive and zero values for the three different cases:

 * return negative values for 
    `input1 < input2`
 * and positive for 
    `input1 > input2`
 * and `0` for equality.

So it is absolutely fine to 
``` Python
    return input1 - input2
```


# Sort array by ordering of another one

I guess there are better ways to do it, but this solution worked for me at least once.
`x` is an array that has got x-values, but not in monotonic order and `y` is the corresponding y-data.
You want to interpolate and hence need to have monotonic ordered data.
```
y = [y for _, y in sorted(zip(x, y))]
x = sorted(x)
```

