Title: Unexpected behaviour of Python's rounding method
Author: Julian
Date: 2017-08-25
Modified: 2017-08-25
Tags: round, bug, numpy, float
Summary:
Lang: en
Status: draft

float64
-------
```
from numpy import float64
example = float64("1.3999780266532955153024886385537683963775634765625000000000000000000000000000000000000000000000000000")
round(example, 2), example, round(1.39997, 2)
(1.3999999999999999, 1.3999780266532955, 1.4)
```

float32
-------
```
from numpy import float32
example = float32("1.3999780266532955153024886385537683963775634765625000000000000000000000000000000000000000000000000000")
round(example, 2), example, round(1.39997, 2)
(1.4, 1.399978, 1.4)
```

float128
--------
```
from numpy import float128
example = float128("1.3999780266532955153024886385537683963775634765625000000000000000000000000000000000000000000000000000")
round(example, 2), example, round(1.39997, 2)
print("{:.100f}".format(example))
```

