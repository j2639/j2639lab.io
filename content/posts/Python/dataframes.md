Title: Pandas and its competitors
Author: Julian
Date: 2023-11-18
Category: Python
Tags: Programming, SQL, Databases, links, pandas, polars, python, dataframes, arrow, duckdb, todo
Summary: Pandas no longer the only tool besides SQL to work on data frames
Lang: en
Status: published

* [pandas](https://pandas.pydata.org/) uses Arrows dataframes since version 2.0
* [polars](https://www.pola.rs/) is written in rust and uses also Arrow dataframes, but also features lazy-evaluation. It can also work on chunks, so in principle you can work on data that is larger than your memory. As it is written in rust, you could also write rust code with it if you like to.
* [duckdb](https://duckdb.org/) is also written in rust, but aims to be utilising SQL syntax
* [Arrow](https://arrow.apache.org/docs/python/index.html) is the base dataframe for newer dataframe systems, but itself is quite limited in what you can do with it, from an analyst's perspective.
* [modin](https://modin.readthedocs.io/en/latest/), [ray](https://github.com/ray-project/ray/) and [dask](https://dask.org/) try to solve problems not only on one computer that can load all the data, but to either load data in chunks to work on data that is larger than your memory or computing that scales to more than one CPU or node.
Modin is 

[Comparison of polars and pandas](https://github.com/FlorianWilhelm/polars_vs_pandas/blob/master/pl_vs_pd.ipynb) by Floraian Wilhelm in form of a Jupyter notebook based blog post.

[DataFrames 2023](https://kestra.io/blogs/2023-08-11-dataframes) is a blog post that compares the aforementioned DataFrame libraries and shows their connections and specific pros and cons for certain usecases. Also it covers [Fugue](https://kestra.io/blogs/2023-08-11-dataframes#fugue) and [Daft](https://kestra.io/blogs/2023-08-11-dataframes#daft) with which I have no experience so far.

[Visual Pandas Selector](https://github.com/manumerous/vpselector) is a library to visualise and interact with pandas DataFrames that consist of time series data. I haven't tested it so far, but it is definitively on my todo list.