Title: venv_jupyter_bug
Author: Julian
Date: 2017-10-12
Modified: 2017-10-12
Tags: bug, python, venv, jupyter, escalation
Summary:
Lang: en
Status: draft


# BUG?

`~/elegant/ANKA/phd/messzeit2/responsematrix/beamline/`

somehow pip run inside the notebook started from within the venv still does show 
packages installed outside of the venv (system packages I think)
```
/usr/bin/python3 -m venv .venv
source .venv/bin/activate
python -m pip install -r requirements.txt
which jupyter -> ...../.venv/bin/jupyter
jupyter notebook
nb: import pip; pip.main(["freeze"]) -> ansible
```


# Bug
When copying a notebook whose name contains a dot the `-Copy` is put at the wrong place---namely before the dot.
Probably the `-Copy1` is inserted after the first dot in the file name though it would be better to put it before the last one.
