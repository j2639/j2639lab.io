Title: Fitting in Python
Author: Julian Gethmann
Date: 2017-02-24
Modified: 2017-02-24
Category: Python
Tags: fit, fitting, regression, Python, data science, science, statistics, reading, links
Summary: How to fit curves to data in Python
Lang: en

Link list to interesting tutorials and libraries:
[iminuit](https://nbviewer.jupyter.org/github/iminuit/iminuit/blob/master/tutorial/tutorial.ipynb)
[kafe](http://www-ekp.physik.uni-karlsruhe.de/~quast/VMroot/addons/kafe/htmldoc/index.html)
