Title: matplotlib_nox11
Author: Julian
Date: 2018-01-18
Modified: 2018-01-18
Tags: matplotlib, ssh, x11, no-x, backend, remote, link, in-a-nutshell
Summary: Use `mpl.use('Agg')` before `pyplot` import for no-X11 environments
Lang: en
Status: published

If you want to plot in an environment without configured X11 you cannot just import `matplotlib.pyplot`, because you will get an error message like
```
no display name and no $DISPLAY environment variable
```
.
But there is a solution if you want to use SSH without X-forwarding, run plotting scripts on a cluster or just in a tmux has by accident not set the `DISPLAY` variable.

You need to set the Backend to `Agg` **before** importing `pyplot`:
```
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
```

So you might want to have a snippet that works remote without X and locally. For this you can check the environment variables
```
import os
if not os.environ.get("DISPLAY", False):
    import matplotlib as mpl
    mpl.use("Agg")
import matplotlib.pyplot as plt
```

via [Stackoverflow](https://stackoverflow.com/questions/4931376/generating-matplotlib-graphs-without-a-running-x-server/4935945#4935945)
