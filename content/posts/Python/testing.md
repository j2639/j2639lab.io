Title: testing
Author: Julian
Date: 2017-07-14
Modified: 2017-07-14
Tags: testing, unittest, pytest
Summary:
Lang: en
Status: draft

Es ist notwending seine Software zu testen.
Python bringt dafür ein paar Tools und Module mit, aber es gibt noch schönere Alternativen zu den Angeboten der Standardbibliothek.

unittest
========

pytest
======
```
import pytest
from package.module import function

def test_function():
    assert function(1, "zwei", 3) == 4
```
```
py.test
```

Test daten
=========
Am Besten hat man die Daten bereits in kleinen Text—Schnipseln, die man einfach direkt ins Python-Script kopiert.
Wenn dies nicht geht, wird es ein bisschen trickreicher, da die Testdateien wiedergefunden werden müssen, auch wenn man Testautomatisierungstools verwendet.

tox
===

GitLab-CI
=========

Github und TravisCI
===================
