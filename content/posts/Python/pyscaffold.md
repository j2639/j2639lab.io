Title: PyScaffold, my boilerplate of choice
Author: Julian Gethmann
Date: 2017-03-08
Tags: Python, boilerplate, scaffold, ruby, ror, blueyonder, intersphinx, sphinx, putup, bumpversion, tox, flake8, mypy, coverage, todo
Summary:
Lang: en
Status: published

This blog post shows an easy way to start packaging and publishing your Python code and especially the [PyScaffold](https://pyscaffold.readthedocs.org) package which can help you a lot when starting a new project.

Setup
=====

First of all one needs to install the package of cause `pip install PyScaffold`.
Then one can begin a new project with the following command:
``` bash
putup -p ProjectName -d "Cool short description for PyPI etc." -u http://github.com/smartsammler/ProjectURL -l new-bsd --with-tox DirectoryName
```

so one does not have to care about [Inter sphinx questions](https://stackoverflow.com/questions/30939867/how-to-properly-write-cross-references-to-external-documentation-with-intersphin) like linking to NumPy.

Beside the necessary files there are some files which are good examples or starting points, like the `tests/test_skeleton.py` and the `ProjectName/skeleton.py`.

One thing you need to do manually is adding all files you want to add to the Git repo.

Required for a valid Python package installable via PIP are 
* `requirements.txt`
* `setup.py`
* `setup.cfg`
* `ProjectName/__init__.py`

Files which require manual adjustments after setting up, but probably before the first commit are
* README.rst
* setup.cfg
* tox.ini
* docs/index.rst

On a regular basis you need to update the
* CHANGES.rst
* requirements.txt
* All your tests and code


Virtual environments
====================

Virtual environments are good for development, testing and for web-environments. They separate the Python runtime from your system.
From Python 3.4 on it virtual environments are easy to use since `venv` is included.
There are three typical commands for environments.
* Create an environment:
```
python3 -m venv .venv 
```
* Join the environment
```
. .venv/bin/source
```
Then you work on your project and if your done you
* leave the environment
```
deactivate
```

If you used to use anaconda you need to leave the environments with `source deactivate` instead of `deactivate`.

Caveats 
-------
Virtual environments have got a problem when the interpreter is updated. So nearly every year you are keen on getting the newest Python with your Fedora, but after upgrading you realize that your virtual environments do not work any more.
Though Python is just a link to the system's Python, the libs live inside of a directory including the Python version.
This is good, but one has to be aware of and upgrade it by hand.
For example your `venv` was created with Python 3.6 and now you are using Python 3.7, you will not find any modules and hence no `pip`.
In `.venv/lib/` you find a directory `python3.6`, but no directory `python3.7`, yet.
I typically try to link the old one, so `cd .venv/lib && ln -s python3.6 python3.7` and update the Python version in `.venv/pyvenv.cfg`.
<!--Then I can create a `requirements.txt` if I haven't already and *reinstall* all dependencies inside of the venv:
```
python -m pip install --force-reinstall -r requirements.txt
```
-->
Often you need to uninstall and install all requirements.
```
python -m pip uninstall -y -r requirements.txt && python -m pip install -r requirements.txt
```

Alternatives/Additions
----------------------
pyenv -- Manage Python versions for virtualenvironments (not just 2 or 3)
pipenv -- Do much more, but also manage environments and dependencies.

Version bumping
===============

`pip install bumpversion`
``` bash
bumpversion [--commit] --allow-dirty --tag patch
```
`patch` or `minor` or `major`. 
For the first time you run bumpversion you must add `--new-version 0.0.1` to provide a starting version.
Be aware that running bumpversion modifies the setup.cfg and deletes all comments. So do your changes before running *bumpversion* since PyScaffold provides useful comments in the `setup.cfg`.
Also be reminded that you also should `git push --tags` from now on to push the tags to the repository.


Code style/flake8
=================

`pip install flake8 tox`
Add flake8 options to the setup.cfg
``` ini
[flake8]
ignore = E501
exclude = .tox/*,docs/*,tests/*
```
which will allow too long lines and igonre PEP-8 errors in the specified directories.

`tox` will run `flake8` or you can run it by `tox -eflake8`

If a specific line should be ignored add `  # flake8: noqa` after it, which will tell flake8 to ignore it. I often use it for the power-operator since I like it to be without spaces surrounding it.
```Python
c = sqrt(a**2 + b**2)  # flake8: noqa
```


Typing/MyPy
===========
(`pip install mypy`)

`mypy --ignore-missing-imports`


Test coverage
=============

Test coverage can be tested with `coverage` (`pip install pytest-cov coverage`)
``` bash
coverage3 tests/
py.test -cov
```

Alternatives and complements
============================

[Python Boilerplate](https://www.python-boilerplate.com) -- A website that generates a minimal version as a Python boilerplate/scaffold.
[Cookie cutter](https://github.com/audreyr/cookiecutter) -- A project written in Python aiming to provide boilerplates for different languages and project types, each based on a git repository as a basis.
[Poetry](https://poetry.eustace.io/) -- Also aims at providing a better packaging experience and is more like pipenv, but also with some basic scaffolding features.
[flit](https://github.com/takluyver/flit) -- Very simple packaging program for example for single file packages.


Further resources
=================

[Official packaging tutorial](https://packaging.python.org/tutorials/distributing-packages/)
[Official user guide](https://packaging.python.org/), and the
[Python Packaging Authority](https://www.pypa.io/en/latest/)
