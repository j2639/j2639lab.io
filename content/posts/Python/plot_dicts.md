Title: Plot dictionaries
Author: Julian Gethmann
Date: 2017-02-24
Modified: 2017-02-24
Category: Python
Tags: matplotlib, dict, in-a-nutshell
Summary:
Lang: en

How to plot data of a dictionarie with `x` as key and `y` as value?

``` python
data = {2007: 1, 2008: 2, 2009: 3}
keys = sorted(data)
values = [data[key] for key in keys]
plt.plot(values)
```
