Title: jupyter_notebook_docker
Author: Julian
Date: 2018-01-16
Modified: 2018-01-16
Tags:
Summary:
Lang: en
Status: draft

When it comes to using [Jupyter](https://jupyter.org) notebooks and [ipythonwidgets](https://ipywidgets.readthedocs.io/en/latest/user_install.html) in virtual environments it easily gets ugly.
The simple `jupyter nbextension enable --py widgetsnbextension --sys-prefix` inside the environment does not always work.
Also using conda and Python virtual environments in parallel is errorprone. 
And of cause using different Python versions in virtual environments and ipythonwidgets may also cause trouble.

Therefore I'm going to separate the complete thing even further than virtual environment by using [Docker](https://docker.io).

Docker installation
-------------------

Container setup
---------------

Using the container
-------------------

Saving state and data
---------------------



