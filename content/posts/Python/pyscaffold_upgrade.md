Title: blog/content/Python/pyscaffold_upgrade
Author: Julian
Date: 2017-12-15
Modified: 2017-12-15
Tags:
Summary:
Lang: en
Status: draft

Upgrade the directory structure of your Python project when switching from PyScaffold 2 to 3.

The new structure is as follows:
```
setup.py
setup.cfg
src/project/__init__.py
```
and not 
```
setup.py
setup.cfg
project/__init__.py
```
as before. 
This has advantages as one can read in these two articles.

How to
======

Move your files from `project` to `src/project`.
Update the `docs/conf.py` to include the new paths. TODO
Update the `setup.cfg` to match the new format (pure setuptools and not bpr anymore.)
