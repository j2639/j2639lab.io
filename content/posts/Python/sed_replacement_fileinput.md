Title: blog/content/Python/sed_replacement_fileinput
Author: Julian
Date: 2017-08-07
Modified: 2017-08-07
Tags: sed, python, fileinput, line-editing, inline, tod
Summary:
Lang: en
Status: draft

sed like behaviour in Python

[fileinput]()
```
import fileinput
for line in fileinput.FileInput("file.txt", inplace=True):
           line = re.sub("^TEST", "!TEST", line, flags=re.I)
           print(line, end='')
       fileinput.close()
```
`inplace=True` does the magic.
