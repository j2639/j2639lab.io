Title: Alarm when job is finished
Author: Julian
Date: 2017-08-06
Modified: 2017-08-06
Tags: todo, bash, Python, in-a-nutshell, server, task
Summary:
Lang: en
Status: draft

The bell
========

ASCII provides a sympol to use the bell every computer has got implemented as a system speaker or more sufisticated.
A super simple alarming that should work on any machine is to print the bell
``` Python
print("\a")
``` 
This of course work within a shell, too:
``` bash
echo "\a"
```
On GNOME and KDE this does not just simply beep once so you have to be there to hear it, but a notification pops out.
To get the notification you must not be in the terminal that raises the bell, so to test you should sleep a second and switch the terminal in that second.


Email
=====

``` bash
echo "Job finished. Yay!" | mail -s "Job finished" email@example.com
```

``` Python
import email
email.
``` 
