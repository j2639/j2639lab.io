Title: Extending Python for better performance
Date: 2017-03
Status: draft
Tags: todo, links, reading

# Links from David Baezley's Office Hour

[Official Python C-Docu](https://docs.python.org/3/c-api/index.html)
[Official extending Python](https://docs.python.org/3/extending/extending.html)
ctypes (old, but needs no extra modules)
[CFFI](https://cffi.readthedocs.io/en/latest/overview.html)
Seem the way to go for different cases
[SWIG](http://swig.org/Doc3.0/SWIGDocumentation.html#Introduction_nn8)
own system, but C++ native
[Cython](http://docs.cython.org/en/latest/)
Does not support common Scientific packages due to missing NumPy, yet
[PyPy](http://pypy.org/download.html#installing-numpy)
[Packaging Extensions](https://packaging.python.org/extensions/)

# Links to C
[libc.pdf](http://www.gnu.org/software/libc/manual/pdf/libc.pdf)
[Extending Python with ...](https://docs.python.org/3/extending/extending.html)
[python - How to pas (stackoverflow)](https://stackoverflow.com/questions/16276268/how-to-pass-a-numpy-array-into-a-cffi-function-and-how-to-get-one-back-out)
http://effbot.org/zone/python-capi-sequences.htm
[The GNU C Library: Trig...](https://www.gnu.org/software/libc/manual/html_node/Trig-Functions.html#Trig-Functions)
[The GNU C Reference Manual](https://www.gnu.org/software/gnu-c-manual/gnu-c-manual.html)
[Using the ffi/lib objects](http://cffi.readthedocs.io/en/latest/using.html#working-with-pointers-structures-and-arrays)
[20. Python C extension](http://book.pythontips.com/en/latest/python_c_extension.html)


# Further links
I haven't read so far https://pythonextensionpatterns.readthedocs.io/en/latest/index.html
