Title: logging
Author: Julian
Date: 2017-08-31
Modified: 2017-08-31
Tags: logging, python, notebook, package, best-practice, module
Summary:
Lang: en
Status: draft

Why
===

How
===

Using modules
-------------
```
import configparser
import logging

config = configparser.ConfigParser()
config.add_section("logging")
config_path = os.path.join(os.path.abspath(os.curdir), "config")                   
config.read(config_path)

logger = logging.getLogger(__name__)
logger.setLevel(getattr(logging, config["logging"].get("level", "INFO")))
filelog = logging.FileHandler(filename=config["logging"].get("filename", __name__ + '.log'))
filelog.setFormatter(logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(message)s"))
logger.addHandler(filelog)

logger.info("Started script")
```

Writing modules
---------------
Add 
```
import logging
logger = logging.getLogger(__name__).addHandler(logging.NullHandler())
logger.info("starting %s" % __name__)
```
to your `__init__` file of the module. [1]
(not sufficient)

Writing applications
--------------------
[2]

Jupyter notebooks
-----------------
```
import logging
logger = logging.getLogger("my_cool_notebook")
logger.setLevel(logging.DEBUG)
filelog = logging.FileHandler(filename="notebook.log")
filelog.setFormatter(logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(message)s"))
logger.addHandler(filelog)
```
will log into a file that is inside the directory where the Jupyter notebook is started (not the location of the notebook) ??.

Ressources
==========
[1] http://docs.python-guide.org/en/latest/writing/logging/
[Official Tutorial](https://docs.python.org/3/howto/logging.html#configuring-logging-for-a-library)
[2] https://12factor.net/logs
https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
http://www.blog.pythonlibrary.org/2012/08/02/python-101-an-intro-to-logging/
