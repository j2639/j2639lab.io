Title: docstrings
Author: Julian
Date: 2017-10-12
Modified: 2017-10-12
Tags:
Summary:
Lang: en
Status: draft

With https://github.com/dadadel/pyment you can convert, at least, the old way formated docstrings (`name - text`) to reST docstrings (`:param name: text`).
But it should be also possible to convert Google and Numpy formats.

