Title: Visualisations and plotting
Author: Julian
Date: 2024-01-10
Modified: 2024-01-10
Tags: plot, vis, links, todo
Summary: Tips and tricks for visualisations in Python
Lang: en
Status: published

# Selection of the right visualisation type
There are a few resources for the selection of the best graph for your purpose that link to code examples for the respective plots.

[from Data to Viz](https://www.data-to-viz.com/) is a website that lets you explore possible common plots for different visualisations. Itself uses a tree visualisation to guide you through your needs to the right plot. Sometimes it can't provide you with links to the right Python library or example code, because its main focus is on providing background to the different plots and tips when and how to use them and when not.

After selecting the right visualisation, the next step could be to click on a link to the [Python Graph Gallery](https://python-graph-gallery.com/) to find out how to implement the visualisation of choice in Python. At this point examles are also provided for different plotting libraries, may it be matplotlib, pandas, seaborn or altair. Though this site's main focus in visualisations with Python, it also provides some backgrounds to some related topics that are not just relevant for one particular library. e.g. cheatsheets, animations, vocabulary, etc.
Both of these sites are by the same Author and hence nicely linked.

# Plotting libraries for static plots (paper, poster)
[matplotlib](https://matplotlib.org/stable/plot_types/index.html) is the defacto standard for static, scientific plots in Python.
[seaborn](https://seaborn.pydata.org/) is based on matplotlib and is optimised to visualise statistical data. That means not just plotting nice distributions, but also taking care of all the error band calculation and visualisation, histograms and Kernels. But it is also a nice short cut to get prettier visualisations than matplotlib's defaults.
[pandas](https://pandas.pydata.org/docs/user_guide/visualization.html) isn't a plottling library per-se, but it provides nicely integrated visualisation features based on matplotlib and thereby often saves a lot of typing. However, if you want to do things not expected to be done, one often has to get back to matplotlib. 

# Plotting dynamic plots (dashboards, websites)
For dynamic plotting there are three main competitors:
* plot.ly,
* bokeh, and
* altair

[plot.ly](https://plotly.com/python/) is developed for multiple languages and nicely integrated into the company's other products, e.g. [Dash](https://dash.plotly.com/) (dashboard).
[bokeh](https://bokeh.org/) is plot.ly's main competitor but started as open source software and is supported by Annaconda and not started as a product of a company and is open source.

[HoloViews](https://holoviews.org/gallery/index.html) is closely interconnected with Bokeh, but also provides plot.ly and matplotlib as backends. It aims to provide interactive plotting experience and streaming plots, e.g. for dashboards and full applications.