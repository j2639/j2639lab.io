Title: packaging
Author: Julian
Date: 2017-09-05
Modified: 2017-09-05
Tags: pip, requirements.txt, setup.py, sphinx, man, jupyter, setup.cfg
Summary:
Lang: de
Status: draft

man pages
=========

Question: How to install the manpages generated with Sphinx?
Es gibt ja so ne files definion (`package_data` von [setuptools](https://setuptools.readthedocs.io/en/latest/setuptools.html#id53)) in der setup.cfg/py mit der man das angehen könnte. Dann einfach nach `/usr/share/local/man/man1`?


requirements.txt vs. setup.py vs. setup.cfg
===========================================

https://caremad.io/posts/2013/07/setup-vs-requirement/

via https://pip.pypa.io/en/stable/user_guide/#installing-packages

tldr: `requirements.txt` für Programme mit festen Versionen (zukünftig vielleicht Pipfile.lock) und setup.py/cfg für Pakete/Bibliotheken mit minimum Paketversionen aber nicht gepinnten.

Using the ``setup.py`` one can add dependencies on archives not published to PyPI. Therefore you need the exact URL to a `tar` or `tar.gz` file and not to a Git-repository.
```
install_requires=["atb"],
dependency_links={"atb": "https://url/to/tar/archive"}
```
e.g. https://git.scc.kit.edu/las/py/cassandra/repository/0.7.1/archive.tar

Furthermore, to my knowledge, it is not foreseen to have two different archives for different Python versions.

In `requirements.txt`-Dateien muss man wenn man öffentliche Git-Quellen nutzt, die man sinnvollerweise mit `http` nutzt, sodass auch Benutzer ohne Account bei der öffentlichen Git-Instanz darauf zugreifen können, `-e ` vor seine URL setzen. Zu mindest bei älteren PyScaffold `setup.py`s, die pbr verwenden.

setup.cfg
---------
Zur Zeit ist der Support von Git-Repos in der (setup.cfg)[https://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files] noch nicht funktionsfähig.

git versioning
--------------
Why should one take care of versioning the package by one self. Beside the bumpversion package, there are alternatives for simple projects, where you do not need to bump the version on your Sphinx project as well.
Namely setuptools' [SCM](https://pypi.python.org/pypi/setuptools_scm) extension and the additional [SCM_Git_Archive](https://pypi.python.org/pypi/setuptools_scm_git_archive) extension.

Wie installiert man lokale Pakete in einer speziellen Version
=============================================================

`pip install --upgrade .==0.0.5` geht nicht, obwohl ja `pip install --upgrade .` geht und `pip install --upgrade pip==1.0` geht.
Ja sogar `$ pip install -e .[PDF]==3.0  # editable project in current directory` geht laut [docu](https://pip.pypa.io/en/stable/reference/pip_install/)

Mit pip kann man jedoch auch Pakete installieren mit nicht so hässlichen URLs. Sprich `pip install git+https://github.com/pypa/pip` funktioniert auch und nicht nur `pip install git+https://github.com/pypa/pip.git#egg=pip` oder `pip install git+https://github.com/pypa/archive/pip..."


PIP als Bibliothek nutzen
=========================

Bis PIP 10 nicht supportet, wie in der [Mailinglist](https://mail.python.org/pipermail/distutils-sig/2017-October/031642.html) nachzulesen ist.
Ab da schon. D. h. jetzt kann man auch aus Jupyter notebooks heraus Pakete installieren, sodass man sicher sagen kann, was im Notebook ist.

Tutorials and good reads
========================
[A very low level, but nice to read intro](https://veekaybee.github.io/2017/09/26/python-packaging/)
[A more elaborate article on publishing on PyPI](https://hynek.me/articles/sharing-your-labor-of-love-pypi-quick-and-dirty/)
http://sedimental.org/the_packaging_gradient.html
