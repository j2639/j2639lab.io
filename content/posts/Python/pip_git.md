Title: pip with git-repositories
Author: Julian
Date: 2017-04-05
Modified: 2017-06-22
Tags: pip, git, python, scm, url, todo, in-a-nutshell, tox
Summary: When to choose which syntax to install your own Python package
Lang: en
Status: published

Jupyter Notebooks
=================

As Jake VanderPlas points out in his [great article on installing packages from Jupyter notebooks](http://jakevdp.github.io/blog/2017/12/05/installing-python-packages-from-jupyter/) the way I was used to do it
`python2 -m pip` not a good way to go.
Instead you need to find and use the right interpreter provided by `sys.executable`
```
import sys
!{sys.exectutable} -m pip install ...
```

Local path
----------
```
! {sys.executable} -m pip install --user --upgrade git+file:///path/to/the/package/.git#egg=package_name
```
Use the exclamation mark for bash mode, then select the interpreter used by the Kernel (via `{sys.executable}) and the `git+file://` and `#egg=package_name`, but with no quotation marks.

requirements.txt
----------------
```
! {sys.executable} -m pip -- install --user --upgrade -r requirements.txt
```
If you are using `ipython` to install packages via `pip` instead of `python` you need to add the `--` to separate the module loading (`-m pip`) and the call of a ipython-script (here none) from the arguments of `ipython`.


The requirements file itself also can have have repositories as dependencies.
```
git+https://github.com/dsavoiu/kafe.git#egg=kafe
```
for example is the dependency in a requirements.txt.
Or with a `-e ` before it to have the latest version always. This is also necessary if you are using https and pbr/older PyScaffolds.
Or with a `@tag_name_or_sha1-hash` between the git and the `#egg=` to explicitly have one specific version (referenced either by the hash of the commit repository or its tag).

In the case of GitLab or Github you might also end your path before the `.git`, but this causes `pipenv` to fail which needs also the `#egg=` and currently is also just capable of dealing with public repos.

Normal Python
=============
Use 
```bash
python -m pip install git+ssh://git@
```

<!-- Public repository
Private repository

SVN

ssh, https, file
-->

Troubleshooting
===============

``` bash
Command "git clone -q ssh://git@reposerver:user/repo.git /tmp/foo/repo.git" failed with error code 128 in None
```
You have either not a proper SSH key uploaded or (in this case) you have forgotten to add a slash between the user name and the colon in the URL.

requirements.txt with requirements specifiers
---------------------------------------------

If you use requirements specifier like 
```
six ; python_version < '3.0'
```
be aware of the impossibility of using them together with the `-e` mode of VCS.

Since I'm not sure if it is a feature or a bug, I have not filed a bug, yet.
