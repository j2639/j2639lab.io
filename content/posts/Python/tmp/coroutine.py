#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Python >= 3.5 required

"""
   :Authors: Julian Gethmann
   :Contact: phd@gethmann.org
   :Date: 2017-05-01
   :Version: 0.2 try-clause
   .. versionchanged:: 0.2
"""
try:
    import asyncio
    Py35 = True
except ImportError:
    Py35 = False

import requests
from cassandra.cassandra import Cassandra, Pvs


if not Py35:
    # run old code
else:
    async def get_cassandra(pv, count=None):
        print(pv)
        HOSTNAME = "ankasr-main.anka.kit.edu"
        PORT = 9812
        count_str = "&count={count}".format(count) if count else ""
        starttime = endtime = 1454698800000000000
        url = "http://{host}:{port}/archive/1/samples/{pv}?start={starttime}&end={endtime}{count_str}".format(
            host=HOSTNAME,
            port=PORT,
            pv=pv,
            starttime=starttime,
            endtime=endtime,
            count_str=count_str,
        )
        failing = True
        err_counter = 0
        while failing and err_counter < 10:
            try:
                resp = requests.get(url)
                if resp.ok:
                    failing = False
            except TimeoutError, ConnectionResetError:
                err_counter += 1
        json = [entry for entry in resp.json() if entry["status"] == "OK"]
        # import pprint
        # pprint.pprint(json)
        if resp.ok:
            print([j["time"] for j in json])
            print([j["value"] for j in json])
        else:
            print("Failed with {url}".format(url))
        return [(j["time"], j["value"]) for j in json]


    def main():
        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.gather(
            get_cassandra(pv=Pvs.pv["fill"]),
            get_cassandra(pv=Pvs.pv["nu_x"]),
            get_cassandra(pv=Pvs.pv["nu_y"])
        ))
        loop.close()


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        print(__doc__)
        sys.exit(0)
    main()
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
