Title: Liste der Farben des Matplotlib-Styles
Author: Julian
Date: 2017-08-01
Modified: 2017-08-01
Tags: matplotlib, colors, style, markers, linestyles, dashes
Summary: Matplotlib Styles und Farben und Marker verwenden und der Vorteil der neuen Cycler
Lang: de
Status: published

Farben verwenden
================

Ab und an kommt es vor, dass man die Farbpalette des gerade in matplotlib verwendeten Styles zu haben.
Beispielsweise, wenn man Scatter-Plots von gelabelten Daten erstellen möchte, wie im folgenden Beispiel
``` Python
for label_idx, (x, y) in data_set:
    plt.scatter(x, y, color=colors[label_idx % len(colors)])
```
.

Um unsere `colors` zu bekommen können wir in neueren Matplotlib-Versionen folgenden Code verwenden:
``` Python
import matplotlib as mpl
colors = mpl.rcParams["axes.prop_cycle"].by_key()["color"]
markers = mpl.rcParams["axes.prop_cycle"].by_key()["marker"]
linestyles = mpl.rcParams["axes.prop_cycle"].by_key()["linestyle"]
```

Früher hieß es nicht `axes.prop_cycle`, sondern `axes.color_cycle`, war dafür nicht so allgemein, sodass man lediglich
``` Python
colors = mpl.rcParams["axes.color_cycle"]
``` 
schreiben musste, dafür nicht noch andere Eigenschaften bekam. Dafür gab es eben nur den Cycler für Farben und nicht auch für Marker und Linienarten.

Der inzwischen verwendete Mechanismus der [Cycler](https://matplotlib.org/cycler/), lässt beispielsweise auch Marker und Farber gleichzeitig rotieren.

Einfachere Variante für Farben
------------------------------
Wenn man nur, wie ich ursprünglich, an den Farben interessiert ist, so gibt es eine noch viel einfachere Variante, die auch [offiziell dokumentiert](https://matplotlib.org/users/colors.html) ist.
Statt sich die Farben zu holen und dann über diese zu itterieren, kann man auch den String `Cx` angeben, wobei `x` durch den Index ersetzt werden muss. Mein erstes Beispiel wird dann zu folgendem:
``` Python
for label_idx, (x, y) in data_set:
    plt.scatter(x, y, color="C{}".format(label_idx))
```
.


Style sheets erstellen
======================

Die entsprechenden Style-Dateien können Cycler enthalten und somit Farben und Marker kombiniert definieren. 
Hier ein kleines Beispiel für die Vorteile von Cyclern:
``` Python
axes.prop_cycle: cycler("marker", ["x", "+", "1", "^", "2", "v", "3", "<", "4"]) + cycler("ls", ["-", "--", "-.", ":", "-", "--", "-.", ":", "-"])  + cycler('color', [ "009682", "4664aa", "A22223", "23A1E0", "DF9B1B", "A3107C", "8CB63C", "A7822E", "FCE500", ])
```
(Diese Farbkombination ist vom KIT geschützt.)

Leider werden z. Z. (matplotlib 2.0.2) noch keine Tupel als Marker unterstützt und man muss alles in eine Zeile schreiben.

Fettnäpfchen
------------

* Man muss alle Cycler in eine Zeile schreiben
* Auch wenn mein Cycler `ls` heißt, so lässt er sich nicht über `ls` als Schlüsselwort abrufen, sondern nur über `linestyle`
* Marker mit Tupeln werden noch nicht unterstützt.
