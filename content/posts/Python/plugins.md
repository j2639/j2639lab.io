Title: Plugins
Author: Julian
Date: 2017-09-02
Modified: 2017-09-02
Tags: plugins, setup.py
Summary:
Lang: en
Status: draft

In the `setup.py`
```
tests_require=['Flask-RESTful[paging]'],
# Install these with "pip install -e '.[paging]'" or '.[docs]'
extras_require={
        'paging': 'pycrypto>=2.6',
        'docs': 'sphinx',
}
```
Then there is a separate `paging.py` that can import the pycrypto package. Of course this file is not part of the `__all__` in the `__init__.py`.

Example from where I got this:
[Flask-restful](https://github.com/flask-restful/flask-restful)

