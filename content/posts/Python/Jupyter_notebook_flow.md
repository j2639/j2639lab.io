Title: Jupyter_notebook_flow
Author: Julian
Date: 2017-08-11
Modified: 2017-08-11
Tags: notebook, jupyter, python, ipython, best practice, tips, git, diff, merge
Summary: 
Lang: en
Status: draft

Since I don't use JupyterLabs or JupyterHub so far, but use Jupyter on different computers and via [SSH tunnel](/ssh forward hostname) from time to time, I have some bookkeeping tricks.

## Show system information and disable auto-saving
My first cell looks like:
```
! hostname; pwd
%autosave 0
import datetime
print(datetime.datetime.now())
import sys
print(sys.version_info)
```
So I always know on which host and in which directory this notebook was used the last time and therefore if I should use it or if it is outdated and I need to use a different one.
Another important information is the Python version, since some of my code needs [libraries that are Python 2 only](rant about beginning projects with Py2).

Last but not least if I run this cell changes in this notebook that break things aren't autosaved and figures in the output stay as long as I want them to be, although I might play with the cells generating them.


## requirements handling
At the moment I don't have a better solution for the problem of dealing with dependencies for notebooks and the fear that inexperienced future users will not be able to figure out what requirements are needed and that the `requirements.txt` in the same directory can be used for it or the jupyter notebooks [requirements including plugin]() is not a solution for me, since I cannot expect them to first install all plugins to get my notebook working. 
And if one wants to email notebooks they must be self-explaining and be runnable without any plugins already being installed.
So I install packages right out of the notebook:
```
import sys
! {sys.executable} -m pip install --user --upgrade git+file:///home/gethmann/scripts/pml_readout/.git#egg=pml_readout
! {sys.executable} -m pip freeze > requirements.txt
```
As noted in this great blog post by Jake van der Plas about [How to install packages in Jupyter notebooks](https://jakevdp.github.io/...) one shouldn't use just ``! python -m pip install``, but the actual used Python.

## Comparing of different notebooks
I use the great (nbdime)[https://nbdime.readthedocs.io/en/latest/vcs.html] for diffing notebooks and a variant of (ipynb_drop_output)[http://pascalbugnion.net/blog/ipython-notebooks-and-git.html] for cleaning git commits.
