Title: Performance
Author: Julian
Date: 2017-03-24
Modified: 2017-03-24
Tags: performance, optimization, profiling, 
Summary:
Lang: en
Status: draft

Performance
===========

Profiling your code
===================

First of all one needs to find the bottle necks by profiling the code (here our `inperformant_script`
``` bash
python -m profile -o . -s TODO inperformant_script.py
```

With the option `-s` one can set the sorting of the output. Good values are in my opinion to sort with *TODO* by providing the argument `TODO`.
