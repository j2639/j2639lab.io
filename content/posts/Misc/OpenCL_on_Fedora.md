Title: OpenCL_on_Fedora
Author: Julian
Date: 2019-11-18
Modified: 2019-11-18
Tags: link-list
Summary:
Lang: en
Status: draft

https://bashbaug.github.io/opencl/2019/07/06/OpenCL-On-Linux.html via https://wiki.tiker.net/OpenCLHowTo
https://ask.fedoraproject.org/t/how-to-enable-opencl/2697

Kernel-Flags:
```
```

Set them via:
```
sudo grubby --update-kernel=ALL --args="radeon.dpm=0"
```
(this only works for Fedora 30+ where `GRUB_ENABLE_BLSCFG` is set in `/etc/default/grub`.

Only boot into multi user target (init 3) and not into graphical (init 5?)
```
sudo systemctl set-default multi-user.target
# sudo systemctl set-default graphical.target
```
