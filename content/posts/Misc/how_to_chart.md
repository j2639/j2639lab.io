Title: What to care about when creating a plot
Author: Julian
Date: 2018-11-20
Modified: 2018-11-20
Tags: gnuplot, matplotlib, checklist
Summary: Things you should check before presenting your plot to your colleagues
Lang: en
Status: draft

From time to time I find my self in the situation that I present my colleagues a plot and they either do not get my message or they correct my plot. 
Furthermore if I see a plot done by a student in a lab course, or colleagues who do not care about it, sometimes I am really shocked how bad a plot can be.

Since I've been in both situations these days, I start to collect some hints on what to check before you use your chart.
This list is by far not complete, but I try to sort it, so you can start at the top and maybe exit a certain point.

Since most of my plots are 

All kind of plots
-----------------
* Label your axes
* Label your axes with dimensions/units
* Let the labels and also the legend not overlap your ticks or even your data
* If you want to compare two similar measures in different plots use the same scaling
* Use exponential notation or bigger/smaller units instead of writing a very long number. e. g. 1 kA instead of 1000 A if your axis range from e. g. 0 to 10 kA.
* Label your axes with units either in parentheses or with a slash
* Use a colour map or colours that fit your medium (lcd, print, beamer, carbon copy), e. g. with a tool like [colorbrewer](http://colorbrewer2.org)

Scatter plot (x-y-plot)
-----------------------
* Use different markers
* Use different markers that are small enough not to overlap, but big enough to be visible
* Don't use a second y-axis, but use two plots which share the same x-axis.

Contour plot
------------
* Use a colour map that has got a big enough colour range and maybe use diverging colour maps instead of linear ones, though there is a linear relation, e. g. if you want to compare (filled) contour plots.
* If your data is linear use a colour map that is perceptually uniform, e. g. have a look at [matplotlib's page about it](https://matplotlib.org/users/colormaps.html)
* As mentioned above for scaling, use the same colour range if you want to compare similar plots
