# Numerical Analysis of Fundamental Frequencies
[Search for the original idea](https://www.google.de/#q=Jacques+Laskar+fourier+transform)
[Fortran implementation](http://dept.astro.lsa.umich.edu/~mvalluri/resources.html)

[Frequency analysis for multi-dimensional systems. Global dynamics and diffusion](http://www.sciencedirect.com/science/article/pii/016727899390210R) by J. Laskar, Physica D, Vol. 67, Issue 1-3, pp. 257, 1993 [DOI](http://dx.doi.org/10.1016/0167-2789(93)90210-R)
