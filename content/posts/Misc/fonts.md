Title: Fonts for the web, command line and editor
Author: Julian
Date: 2023-11-18
Modified: 2023-11-18
Tags: font, cli, web, links
Summary: Links to fonts
Lang: en
Status: published

Fonts for the web
=================
If you do not want to use webfonts, but use system fonts, you can find a nice overview of how you should write your CSS font declarations for specific needs to use system fonts of the most common systems.

https://modernfontstacks.com/

Besides the list of font names for the different systems, you can read a text on your system with different font types.

Accessible fonts
----------------
The [Atkinson font](https://brailleinstitute.org/freefont) is a font designed for low vision readers—not only for the web.

Fonts for your terminal and text editor
=======================================

A new [monospace font family](https://monaspace.githubnext.com/) has been released by Github.
Monaspace is not just one font, but a whole family, so you can use different monospace fonts for different parts of your code, e.g. comments and the main code.
Also it features a cool feature called “texture healing”. That is the widening of characters in case the neighbouring character is small, like an `m` next to an `i` will widen the `m` and make the `i` smaller though staying in its overall bounding boxes. More on that also on their website.