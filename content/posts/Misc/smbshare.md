Title: smbshare
Author: Julian
Date: 2017-07-06
Modified: 2017-07-30
Tags: smbshare, samba
Summary: Download directories from samba shares
Lang: en
Status: published

Sometimes I need to download a directory with the smbclient. And since I then need to [duckduckgo](https://duckduckgo.com/?q=smbclient+download+directory) it which shows me the first [superuser](https://superuser.com/questions/856617/how-do-i-recursively-download-a-directory-using-smbclient) answer, I copied it to have a backup if superuser.com might be offline:
```
    smbclient '\\server\share'
    cd 'path\to\remote\dir'
    lcd '~/path/to/download/to/'
    mask ""
    recurse ON
    prompt OFF
    mget *
```
Connect to the `\\server\share`.
Go to the directories you want to download and locally where you want to download to. 
set recursion on, like `scp`'s `-r`. Disable asking you every time if you really want to download it by setting the prompt to off (like `-y` in many other GNU/Linux programs). And finally multi-get all files with `mget *`.
