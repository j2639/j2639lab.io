Title: Eject eSATA disks
Author: Julian
Date: 2018-01-29
Modified: 2018-01-29
Tags: esata, sata, hard disk, mount, umount, eject, remove, poweroff, in-a-nutshell
Summary: Different states of ejecting a harddrive
Lang: en
Status: published

Find out which drive you are going to eject:
============================================

```
dmesg|tail
```
shows which drive you inserted newly. So newly attaced USB drives or eSATA drives show up as something like `[sdb]` where the `b` is the letter which may vary under Linux. 

```
mount
```
often does show all your mounted drives, which in some cases can be helpfull, but in general is a mess and not helpful if you haven't mounted your drive, yet. You then need to know where your drive is mounted and can figure out the drives name from that.

```
df -h
```
is less messy, but also only works if your drive is mounted. You do not need to know where it is mounted, but the size of you drive or the partitions to be more precise.

```
sudo fdisk -l
```
does work on most of the systems, but you need `sudo` rights, because this tool is ment for formating.
You get a lot of information of the drive from which you should be able to figure out which drive is which. 


Ejecting the drive
==================

If you want to eject a harddrive that is not hotswap capable, but plugged into a eSATA bay, you have several options.

A normal ``sudo umount /dev/sdb`` (`sdb` is my drive, but yours may have another name) will not work if your drive hasn't been mounted before. So if you just formated your drive it does not work.

``sudo eject /dev/sdb`` does eject the drive, so it is not possible to remount it easily, since the USB-bus for this drive is removed.
``sudo udiskctl power-off -b /dev/sdb`` powers down your drive, so you should be able to detatch it physically.

If you have got more useful tips related to this topic, get in contact with me, please.
