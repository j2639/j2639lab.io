Title: ssh forward to link local
Author: Julian
Date: 2017-04-12
Modified: 2017-04-12
Tags: ssh, link local, localhost, 127.0.0.1, notebook, ipython, python
<!-- Summary: -->
Lang: en
Status: published

At some point I used Jupyter notebooks on different computers and with different Python versions and wanted to distinguish them.
So I gave them IP addresses with the Python version encoded in them and the host all within the local block `127.`.

But how do I ssh-forward to them now?
``` bash
local.host:~$ ssh remote.host -L 8000:127.1.3.6:8888 
remote.host:~$ jupyter notebook list
Currently running servers:
http://127.1.3.6:8888/ :: /home/user/path/to/notebook
```
does the trick. On the `local.host` I forward not like one is used to to `localhost`, but to `127.1.3.6`.
I get this address, if I do not already know it, by listing all Jupyter notebooks.

On my local computer I then have to go to `http://localhost:8000` and not to `http://127.1.3.6:8000` which one may have expected.

## ssh through hops
In case your Jupyter notebook is running behind a firewall with a login gateway, you can also use this method.

``` bash
local.host:~$ ssh -L 8000:localhost:8800 gateway.host ssh 8800:127.1.3.6:8888 jupyter.host -N
```
This chain can be extended, but you have to (re)move the terminating `-N` accordingly.
