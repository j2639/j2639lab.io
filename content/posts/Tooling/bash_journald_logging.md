Title: Logging from a shell to the journald
Author: Julian
Date: 2017-03-03
Modified: 2017-03-08
Category: Tooling
Tags: bash, sh, journald, linux, systemd, log, logging, todo
Summary: Logging into the journald is just pipeing your echoed message to systemd-cat
Lang: en
Status: published


Log to journald
===============

I created a very tiny logging function for shell and bash to log into the journal.
Since it may be updated, I created a [GitHub Gist](https://gist.github.com/smartsammler/3c287108a4f76998f363f30267f021ee) out of it.

Further reading
===============

The `systemd-cat` man page.


Log from Python to journald
===========================

In Python it is as easy, just import journal from [systemd](https://www.freedesktop.org/software/systemd/python-systemd/journal.html)
``` Python
from systemd import journal
journal.send('Hello world')
journal.send("Hi", MESSAGE_ID="me", PRIORITY=1)
```
And of course it is possible to use it together with the logging system
``` Pytho
log.addHandler(JournalHandler())
```
