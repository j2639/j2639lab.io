Title: blog/content/Tooling/btrfs_disc_move
Author: Julian
Date: 2018-03-01
Modified: 2018-03-01
Tags:
Summary:
Lang: en
Status: draft

In this article I use the `# command` for showing that I'm acting as root (`sudo -s`) and the `command` is being executed whereas the lines that follow without a `#` are *my* output, so you can get a glimpse where to look for.
If the output is not of interest I do not include it. Of cause you should read your output and stop when errors occur.

I want to move my btrfs volumes from my old slow disk (`sdc4`) to a newer faster disk (`sdb4`).
Only the `/`, `/usr/local` and `/home` are subvolumes of the btrfs I am using.
This information can be extracted from `btrfs sub list /` searching for the first entries which should have the `top level 5`.
```
# btrfs sub list /
ID 257 gen 174550 top level 5 path root
ID 258 gen 174554 top level 5 path home
ID 259 gen 174539 top level 5 path usr_local
...
```

`swap`, `/boot`, and `/boot/efi` are not btrfs, which explains the partition number of 4.

Create a `btrfs` on the new partition (I used KDE's partitionmanager, but e.g. fdisk should work fine as well).

I create read-only snapshots of the volumes, because `btrf subvolume send` can only handle read-only volumes.
```
# btrfs sub snap -r / /root_ro
# btrfs sub snap -r /usr/local_ /usr_local_ro
# btrfs sub snap -r /home /home_ro
```

Then I mount the new device which at this point does not have any subvolumes.
I show the following part just for the `root` volume, but I repeated them later on for the others as well.
```
# mkdir /mnt/sdb4
# mount /dev/sdb4 /mnt/sdb4
```

Now I can "copy" the subvolumes to the new disk using the send-receive mechanism.
```
# btrfs send /root_ro | btrfs receive /mnt/sdb4/
```

I copy/move the subvolumes to their correct names. This also fixes the issue, that the received subvolume is read-only.
```
# btrfs sub snap /mnt/sdb4/root_ro2 /mnt/sdb4/root
# btrfs sub del /mnt/sdb4/root_ro2
```

You can test if that worked out by
```
# umount /dev/sdb4
# mount -t btrfs -o subvol=root /dev/sdb4 /mnt/sdb4
bin boot dev ...
# umount /dev/sdb4
```

Now redo the procedure for `/usr_local_ro` and `/home_ro`.

Finally it is time to adjust the file system table and the boot loader.
```
# vim /mnt/sdb4/etc/fstab
```

[Erroneous tutorial](http://c0rp.kz/moving-root-btrfs-subvolume-to-another-disk/)
[Kubuntuforums.net](https://www.kubuntuforums.net/showthread.php/67487-More-btrfs-fun-Moving-a-subvolume-from-one-drive-to-another)
[Arch wiki](https://wiki.archlinux.org/index.php/Btrfs#Send.2Freceive)
