Title: X11-forwarding-issues
Author: Julian
Date: 2022-05-02
Modified: 2022-05-02
Tags: x11, wayland, ssh, tmux
Summary: How to fix issues with X11 forwarding
Lang: en
Status: draft


The first problem I faced was that for some reasons my tmux did not set the `$DISPLAY` variable for new tabs (maybe the last connection was without X).
However, I could show matplotlib plots there. Maybe they were using Wayland and thus were not requiring the `$DISPLAY` variable set correctly.

My workaround was to open a new terminal where the `$DISPLAY` environment variable was set correctly to `:0`.


My second problem I faced was that on the remote host I got the error message
> No protocol specified

which was probably caused, because the X11 was not set up correctly.
Only exporting the DISPLAY variable did not help (neither for `:0` so the remote computer's display, nor `:10` the first assumed SSH-display.
However, setting the DISPLAY variable to `:10` resulted into the error message disappearing, though no graphical program was showing up either.

As described in [a blog post](https://akashmittal.com/xauthority-not-exist/), I had to set up my `.Xauthority` file like
```
touch ~/.Xauthority
xauth generate :10 . trusted
```

Further common things on new remote hosts are disabled SSH-X11-forwarding policies.
On the remote host you must have set
```
X11Forwarding yes
```
in your `/etc/ssh/sshd_config` and on your local host, you might have to enable
```
ForwardX11 yes
```
in your `~/.ssh/config`, too.
