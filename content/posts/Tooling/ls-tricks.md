Title: ls-tricks
Author: Julian
Date: 2017-03-10
Modified: 2017-03-10
Tags: in-a-nutshell, ls, list, gid, uid, id, except, without, bash, zsh
Lang: en
Status: published

Tricks with ls
==============

List files except those with a specific extension (zsh)
-------------------------------------------------------
To list all files in the current directory except those with special extensions often is very useful. 
For example if I am in a directory with many plots or many data files and only a few others in which I am interested in, I am happy to use zsh, but I need to remember the following syntax
``` bash
ls *~*(txt|dat)
``` 
which will list all, but files not ending with `txt` or `dat`. If one only wants the files not ending with `txt` the braces and the or-pipe is not to been used.

List files with their UID and GID
---------------------------------
For debugging NFS or FUSE mounting issues I sometimes need to know who's file the file is I am looking at, but not only like with a normal `ls -l` the name and group, but the user id and the group id.
To achive this one does not need to use `ls`, but `stat` which gives other/more information about a file
``` bash
stat -c %u:%g *
```
