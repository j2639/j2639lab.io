# What features I miss in KDevelope

I'm coming from vim and I want to give KDevelope a try for Python development.

I miss the following nice things from vim

 * `Ctrl + Shift + O` to jump back to the previous position
 * KDevelope does not import the Key mappings in the insert mode correclty
 * Y does not work only 0y
