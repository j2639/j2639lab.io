Title: windows-ssh
Author: Julian
Date: 2022-03-17
Modified: 2022-03-17
Tags: Windows 10, ssh
Summary:
Lang: en
Status: draft

Good to know: Windows 10 supports its own OpenSSH fork

But you have to activate/install it as described at https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse

As administrator run the Powershell command
```Powershell
Get-WindowsCapability -Online | Where-Object Name -like 'OpenSSH*'
```
and then install the client
```Powershell
Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
```
and maybe the server
```Powershell
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
```
and if you don't want to have it anymore, you can uninstall it
```Powershell
Remove-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
```.

*Note*:
ssh-copy-id doesn't ship with it, so you have to create your own script.
Open `$profile` in your favourite editor and [add the script](https://www.conradakunga.com/blog/copy-ssh-key-in-windows-10/)
```Powershell
function ssh-copy-id([string]$userAtMachine, [string]$port = 22) 
{   
    # Get the generated public key
    $key = "$ENV:USERPROFILE" + "/.ssh/id_rsa.pub"
    # Verify that it exists
    if (!(Test-Path "$key")) {
        # Alert user
        Write-Error "ERROR: '$key' does not exist!"            
    }
    else {	
        # Copy the public key across
        & cat "$key" | ssh $userAtMachine -p $port "umask 077; test -d .ssh || mkdir .ssh ; cat >> .ssh/authorized_keys || exit 1"      
    }
}
```
that can be used similarly to ssh-copy-id on Linux: `ssh-copy-id user@host [port]`.
