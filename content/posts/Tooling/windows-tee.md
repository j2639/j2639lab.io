Title: Windows PowerShell's tee
Author: Julian
Date: 2022-03-22
Modified: 2022-03-22
Tags: Windows, PowerShell, pipes
Summary: Windows PowerShell pipe syntax for tee in comparison to Linux'
Lang: en
Status: draft

On Windows there is no grep and tee, but the PowerShell offers a Tee-Object.

In order to tee your stdout and errout into a file, you can use the following syntax
```
command-with-output-or-errors 3>&1 | Tee-Object -FilePath outputfile.txt
```
.
So in contrast to Linux/UNIX `tee` requires the flag `-FilePath`.
Furthermore piping of standard error is different from the POSIX systems.
On Windows you need to pipe the third channel into the first `3>&1` instead of the second `2>&1`.

