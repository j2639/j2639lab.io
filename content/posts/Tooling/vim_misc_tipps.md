Title: vim_misc_tipps
Author: Julian
Date: 2017-09-04
Modified: 2017-09-04
Tags: vim, regex, tipps, in-a-nutshell
Summary:
Lang: de
Status: draft

RegEx-Sammlung
==============

Ersetzte die von mir bisher verwendete Variante Farben in `matplotlib` anzugeben mit der vor kurzem entdeckten `C#`-Variante:
```
:%s:color=colors\[\([0-9]\+\)\]:color="C\1":gc
```
