Title: Dotfiles
Author: Julian
Date: 2018-05-16
Modified: 2018-05-16
Tags: zsh, bash, login, dotfiles, profile, 
Summary:
Lang: en
Status: draft

Which files are called when?
============================
[Article that explains why bash_profile and why bashrc](http://www.joshstaiger.org/archives/2005/07/bash_profile_vs.html)

> Commands are first read from /etc/zshenv; this cannot be overridden. … Commands are then read from $ZDOTDIR/.zshenv. If the shell is a login shell, commands are read from /etc/zprofile and then $ZDOTDIR/.zprofile. Then, if the shell is interactive, commands are read from /etc/zshrc and then $ZDOT-DIR/.zshrc. Finally, if the shell is a login shell, /etc/zlogin and $ZDOTDIR/.zlogin are read.
[Article on how to speed up a shell](https://blog.jonlu.ca/posts/speeding-up-zsh)

zsh
---
.zshrc
.paths
.aliases
.functions (my special alias)
.sensitive (my hosts specifics)
.exports (my exports)

bash
----
.bash_profile  (called at each login, created by Fedora)
.profile
/etc/profile.d/*
.bashrc (created by Fedora)
/etc/bashrc
.bash_logout (created by Fedora)
.bash_history (created by Fedora)

Unordered
---------
.zshenv  (I deleted it)
.zlogin  (I deleted it)
.zsh-update (contains the last update of the oh-my-zsh and therefore should be untouched)
.zsh_history (see `.histfile`)
.zshrc (created by Fedora if zsh is installed)

.login (I deleted it)
.bash_history


Keeping dotfiles under version control
======================================

[Great article about keeping track of dotfiles](https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/)
tldr:
Once do
```
git init --bare .dotfiles
git remote add github git+ssh://github.com/smartsammler/dotfiles
alias config="git --git-dir=.dotfiles --work-tree=$HOME"
config config --local status.showUntrackedFiles no
```
On each new node do (after deleting/moving to \*{,-old} the hidden files that are already there)
```
git clone --bare git+ssh://github.com/smartsammler/dotfiles
alias config="git --git-dir=.dotfiles --work-tree=$HOME"
config config --local status.showUntrackedFiles no
config checkout
```
On daily basis do 
```
config add .vimrc
config commit -m "Add new cool plugin to run emacs within vim"
```
