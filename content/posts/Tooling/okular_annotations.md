Title: Okular_annotations
Author: Julian
Date: 2018-06-12
Modified: 2018-06-12
Tags: okular, kde, pdf, annotations
Summary:
Lang: en
Status: draft

There are two issues when it comes to annotations and Okular.

Firstly, okular does not show annotations in PDFs by default. One has to double click the annotated text to actually view the annotation text.

The second one is that your annotations are saved locally and not inside the PDF by default.
To circumvent this behaviour you need to save the file again using "Save As".

