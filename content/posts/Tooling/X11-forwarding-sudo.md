Title: X-forwarding combined with sudo
Author: Julian
Date: 2017-03-10
Modified: 2017-03-10
Tags: sudo, x11, x-forwarding, in-a-nutshell
Lang: en
Status: published

X11 forwarding combined with sudo
=================================

Unfornunately I have no better solution found for X-forwarding and `sudo`ing afterwards, yet.

This solution is insecure, but works as a dirty hack.
``` bash
user:~$ xauth list | grep unix$(echo $DISPLAY | cut -c10-12) > /tmp/xauth
user:~$ sudo -s
root:~# xauth add $(cat /tmp/xauth)
root:~# rm /tmp/xauth
```
