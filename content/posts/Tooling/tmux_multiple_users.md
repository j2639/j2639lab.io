Title: tmux_multiple_users
Author: Julian
Date: 2019-11-18
Modified: 2019-11-18
Tags: tmux
Summary: Use tmux with multiple users
Lang: en
Status: draft

Unless with screen with tmux it is not only possible to use one session for multiple users sharing the same pane,
but also to let them work independently.

The screen case that is good for e. g. pair programming can be achieved by joining the tmux session as follows
``` bash
tmux a -t session-name
```
.
If you want to use the panes independently, e. g. if you want to work in parallel, then you have to create a new session
``` bash
tmux new -t session-name
```
, but also set the `-t` flag to the same session that one wants to join.


[tmux cheat sheet](https://gist.github.com/henrik/1967800)
