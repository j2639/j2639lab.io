Title: tmux_multiple_hosts
Author: Julian
Date: 2019-05-02
Modified: 2019-05-02
Tags: tmux, in-a-nutshell
Summary: Run a command on several hosts
Lang: en
Status: draft

To run a command on several hosts without typing it that often, you can open all the hosts in different panes in one tmux window and then run the command in all panes in parallel.
To do the latter, you need to type
```
:setw synchronize-panes
```

After you have done your operations re-run that command and you can do different things in the different panes again.

Via [unix.sx](http://unix.stackexchange.com/questions/298702/ddg#328413)


Warning
-------

You should not write `sudo` commands or other commands that demand passwords, because the input is written to the screen then.
