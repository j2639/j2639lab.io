Title: online-tools
Author: Julian
Date: 2021-08-25
Modified: 2021-08-25
Tags: web2.0, tools, remote, organisation, gtd, office
Summary: Some online tools that are useful for remote work
Lang: en
Status: draft

# Flowcharts

[draw.io](https://app.diagrams.net/) is an app to create flowcharts online in a GUI. Diagrams can be saved to Github/Gitlab and online drives or your local hard disk.
It's good for programming tasks, e.g. UML, flowcharts, cloud infrastructure, and even circuits. Furthermore it includes mind maps and business charts.

It is cloud based, not OSS, but the drawings can be exported as SVGs.
An offline electron app exists, too.


# Whiteboards
## Commercial/Free for personal use

## OSS

## Not so cool:
* https://www.notebookcast.com/ — very basic and only export to PNG option, but no vector graphics nor save/load functionality. Collaboration not tested.

# Gathering

* gather.town — build your office, very in-formal 
* wonder.me — bubbles instead of rooms, no special features like whiteboards (afaik)

## OSS
* rc3 ?

# Video conferencing

* MS teams (commercial, privacy unfriendly)
* Zoom (commercial, privacy unfriendly, free to use)
* Webex (?)
* Google meet, for pure video chat quite nice
* alpha (Karlsruhe based start up)
* Discord

## OSS
* meet.Jitsi.org
* Big Blue Button (?)
* Matrix + Jitsi tries to be like MS teams/Discord (?)


# Chat
* Slack
* Discord

* Matrix
* Mattermost integrates with Gitlab

# Text
## LaTeX
overleaf.com is a collaborative commercial LaTeX editor.
sharelatex is an good OSS alternative to overleaf.com.

## Markdown
CodiMD/[hedgedoc](https://hedgedoc.org)

## Richtext
etherpad: piratenpad

# Spreadsheets

# Notebooks
Google collab
Github+Jupyter+B

# Editors/IDEs
* PyCharm can collaborate/share the screen
* VSCode can share the screen (though VSCodium cannot)
