Title: rip grep and other new cli tools
Author: Julian
Date: 2017-08-07
Modified: 2023-11-18
Tags: rg, rust, grep, rip, todo, gitui, tig, in-a-nutshell
Summary: Fast alternative to grep
Lang: en
Status: published

Fancy new command line tools
============================

In the past years many new command line tools were delveloped. Mostly in rust and go, but sometimes also in other languages like Python.
Often they aim at being either 
* very fast or 
* more interactive than their old UNIX competitors and
* with better support for currently used data formats like JSON

ripgrep
-------
A super fast alternative to grep which I often use is [rip grep](https://github.com/BurntSushi/ripgrep/).
It is written in rust and it has got some features which are not that prominent in grep.

```
rg --type-add 'elegant:*.{lte,ele,log}' -telegant BEND
```

tig and gitui
-------------
Though git itself is also not too old, it suffers from its bad user experience (UX).
tig and gitui try to provide a interactive (as in ncurses) user interface.
[gitui](https://github.com/extrawurst/gitui) is faster than tig

Others
------
Other such cool tools include
* `fd` instead of `find`
* `mosh` instead of `ssh`
* `ncdu` instead of `du`
* `jq` for JSON parsing

Editors
=======
* I haven't tested [helix](https://helix-editor.com/) yet, but it claims to be a fast terminal text editor somewhere between neovim and VS Code.

Terminals
=========
* [warp (ref link)](https://app.warp.dev/referral/7MGZ4Z) is an AI powered terminal (where you have to register atm). At first this sounds strange, but it is quite convenient to be able to search for common usage of some cli tools. So instead of hitting `^R` for your previously used command, you can type `cmd+\``. It also features mouse support.