#!/usr/bin/env bash
#
# Script to backup your home directory to a local disk bi-hourly and to our server, which is backuped weekly, weekly.
# Additional functions to setup or get help about borg and an alternative backup strategy using rsync.
# borgbackup needs to be installed both on the client and on the server. So for untrusted off-site backups you need to mount as local volumes, but that is not the case here, anyhow.
# 

log() {
  # log to journal
  script_name=$(basename "$0")
  echo "$1" | systemd-cat -t "$script_name" -p "${2:-"info"}" \
          || echo "Error in script $0" | systemd-cat -t "$script_name" -p err
}

help() {
  # print help and usage
  cat <<EOT
    Examples of useful borg commands:
    ---------------------------------
    borg list gethmann@129.13.238.117:/home/gethmann/backup
    borg info ssh://gethmann@129.13.238.117/home/gethmann/backup/::las113.las.kit.edu-2017-09-19
    borg mount ssh://gethmann@129.13.238.117/home/gethmann/backup/::las113.las.kit.edu-2017-09-19 /mnt/backup
    borg umount /mnt/backup
    
    Initial setup of a new borg repo
    ---------------------------------
    borg init --encryption=none /run/media/gethmann/fedora/home/gethmann/backup

    # Setting this, so you won't be asked for your repository passphrase:
    export BORG_PASSPHRASE='XYZl0ngandsecurepa_55_phrasea&&123'
    # or this to ask an external program to supply the passphrase:
    export BORG_PASSCOMMAND='pass show backup'
    # crontab -e to edit the cronjob for your user

    Usage of this script
    --------------------
    There is an excludelist file at ~/bin/excludelist which is used to exclude files.
    Furthermore you can put a ".nobak" file into the directory which should not be backuped, e.g. "touch ~/.ssh/.nobak"
    To run this script you can provide the environment variables \${SERVER} with the server's IP/hostname and a varible \${REPOPATH}

    $0 {-h|--help} 		show this help 
    SERVER=backup.las.kit.edu REPOPATH=/las-archiv1/users/${USER}/backup/borg/ $0 --init 	initializes a new remote repository
    $0 --init /backup/borg/  	initializes a new local repository
EOT
}

init() {
  # initalize a new backup for a different user
  REPOPATH=${REPOPATH:-/las-archiv1/users/${USER}/backup/borg/}
  REPOSITORY=${1:-${USER}@${SERVER}:${REPOPATH}}
  if [[ "$REPOSITORY" = *":"* ]]; then
    ssh ${USER}@${SERVER} -c "mkdir -p \"${REPOPATH}\"" 
  fi
  borg init --encryption=none ${REPOSITORY}
}

run() {
  # check if repo exists or mount it
  if [[ ! -d "$REPOSITORY" && "$REPOSITORY" != *":"* ]]; then
      log "Borg backup failed, because repo path does not exist and could not be mounted or looks like a remote one." err
      log "Please run 'sudo mkdir -p /run/media/gethmann/fedora && sudo mount /dev/disk/by-label/fedora /run/media/gethmann/fedora/'." err
  fi
  
  # Backup all of /home and /var/www except a few
  # excluded directories
  	# --compression lzma,9                    \
  echo "Creating new borg backup ..."
  ionice -c 3 -n 7 \
	borg create -v --stats                          \
  	--exclude-if-present .nobak             \
  	--one-file-system                       \
  	--compression lz4                    \
  	--exclude-from ~/bin/excludelist	\
      $REPOSITORY::'{hostname}-{now:%Y-%m-%dT%H}'    \
      ${HOME}                              
      # general
      # python
  
  # Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
  # archives of THIS machine. The '{hostname}-' prefix is very important to
  # limit prune's operation to this machine's archives and not apply to
  # other machine's archives also.
  echo "Deleting old borg backups..."
  if [[ "$REPOSITORY" != *":"* ]]; then
    ionice -c 3 -n 7 \
	borg prune -v --list $REPOSITORY --prefix '{hostname}-' \
        --keep-hourly=12 --keep-daily=7 --keep-weekly=4 --keep-monthly=6 --keep-yearly=10
  else
    ionice -c 3 -n 7 \
	borg prune -v --list $REPOSITORY --prefix '{hostname}-' \
        --keep-hourly=12 --keep-daily=7 --keep-weekly=4 --keep-monthly=12 --keep-yearly=10
  fi
}

rsync_backup() {
    pushd -
    SERVER=129.13.238.126
    DEST=/las-archiv1/users/${USER}/backup/
    rsync --progress --relative --archive --compress --delete --delete-excluded \
          --exclude-from=${HOME}/rsync_exclude.txt ${HOME} ${SERVER}:${DEST} \
  	&& echo "Weekly backup went well"
    popd
    touch ~/bin/last.backup
}

main() {

  log "Borg backup started" info

  #REPOSITORY=gethmann@129.13.238.117:/home/gethmann/backup/
  #REPOSITORY=/run/media/gethmann/fedora/home/gethmann/backup/
  SERVER=${SERVER:-129.13.238.126}
  REPOPATH=${REPOPATH:-/las-archiv1/users/gethmann/backup/borg/}
  #run

  # Friday and 
  # $(date +%w) == 5 && \
  if [[ $(date +%F) != $(stat --format=%y ~/bin/last.backup | cut -f1 -d" ") ]]; then
    printf "Weekly backup (once)" # via rsync)"
    REPOSITORY=sg7149@${SERVER}:${REPOPATH}
    printf " to %s\n" $REPOSITORY
    run
    # rsync_backup
  else
    log "Skipping backup, because it's not Friday."
  fi

  log "Borg backup finished successful" info
}

# [ "$#" -gt 0 ] && [ "$1" = "-h" -o "$1" = "--help" ] && help && exit 0
[ "$#" -gt 0 -a "$1" = "--init" ] && init "${2}" && exit 0
[ "$#" -gt 0 ] && help && exit 0

main

