Title: DNSrecords
Author: Julian
Date: 2022-02-22
Modified: 2022-02-22
Tags:
Summary: DNS entries (SSHFS)
Lang: en
Status: published

There are standard DNS entries like the `A` or `AAAA` record and the `CNAME`.
However, there are many more of which I will present some here.

I start with the SSHFP, how to set it and how to use it.

SSHFP
=====
The SSH finger print record is for convenient and secure SSH login to "foreign" computers.
So instead of checking the SSH finger print of a computer to which you want to SSH (or typing `yes` without check the first time you ssh to that computer), you can provide the SSH fingerprint in DNS.

Server side
-----------
On the respective computer you can generate the full DNS entry by typing
```
ssh-keygen -r /etc/ssh/ssh_host_ed25519_key.pub
```
and selecting the one of your choice.

The correct path might vary depending on your distribution, like in the examples above for the ED25519 and below for the ECDSA keys.

Then the correct entry, as stated in the [Wikipedia](https://en.wikipedia.org/wiki/SSHFP_record) is
```
host.example.com.  SSHFP 3 2 b5fd5d77521104ae99b2414c476aa9bfed6aaaba245971214eabc0e7e20a86c5
```
where the hostname must be the correct one (here `host.example.com`) including the `.` as always for DNS entries, then `3` can be either 1: RSA; 2: DSA, 3: ECDSA; 4: Ed25519 6:Ed448. As we used the ecdsa public key file, we should use 3 in the example.
The next number (`2`) is for SHA-256 and can be `1` for SHA-1 which one could select with ssh-keygen's `-E` option.

The final alpha numerical string with +-signs is the hash/finger print you got as a return value from ssh-keygen.
The hash may need to be transformed into the correct format.

| Encryption | Number |
| :-: | :-: |
| RSA | 1 |
| DSA | 2 |
| ECDSA | 3 |
| ED25519 | 4 |
| Ed448 | 6 |

| Hash | Number |
| :-: | :-: |
| SHA-1 | 1 |
| SHA-256 | 2 |

*NB*: SHA-1 is broken theoretically and with a first actual collision in 2017.

Caution:
~~~~~~~
```
ssh-keygen -lf /etc/ssh/ssh_host_ecdsa_key.pub
```
shows the fingerprint in a format, like you are shown it when asked to accept the fingerprint for the first SSH connection.
However, that format is not suitable for the DNS entry!

The `-lf` option shows the hash base64 encoded, whereas the DNS entry must be hex encoded.

In Python you can convert the base64 encoded string to the hex encoded one with the following code:
```Python
>>> import base64
>>> lf = "tf1dd1IRBK6ZskFMR2qpv+1qqrokWXEhTqvA5+IKhsU=".encode()
>>> base64.b64decode(lf).hex()
b5fd5d77521104ae99b2414c476aa9bfed6aaaba245971214eabc0e7e20a86c5
```
Here I filled the missing length with the `=`. This solves in the case of `Incorrect padding` errors.

Client side
-----------
In your SSH client you have to provide the option to check the DNS record:
```
-o VerifyHostKeyDNS=yes
```
. Unfortunately, this does not work on Windows with Putty.
In your SSH configuration you can put the option
```
    VerifyHostKeyDNS yes
```
.
To look it or check if it already propagated correctly, you can run
```
dig +dnssec sshfp host.example.com +short
```
.
