Title: tmux copy and paste
Date: 2017-02-24 10:01
Modified: 2017-02-24 10:01
Category: Tooling
Tags: tmux, copy, paste, in-a-nutshell
Slug: tmux-copy-paste
Authors: Julian Gethmann
Summary: Ctrl+[; Space; Enter; Ctrl+]
lang: en


tmux copy and paste
===================

Since I always need to search for it, if I need it and not just for scrolling:

 * <prefix> [  # start copy mode
 * Navigate to start of the block to be copied
 * <Space>  # begin selection
 * <Enter>  # end selection/copy
 * <prefix> ] # paste

Or if one is in the *emacs* mode and does not have `vi` in the `EDITOR` or `VIEWER` system variable the inner secquence would be

 * <Ctrl> + <Space>  # begin selection
 * <Meta> + g  # end selection/copy


Configuration change
--------------------
With the following config one can use a more vim like copying

```
set -g mode-keys vi
unbind p
bind p paste-buffer
bind -t vi-copy 'v' begin-selection
bind -t vi-copy 'y' copy-selection
```

 * <prefix> [  # start copy mode
 * Navigate to start of the block to be copied
 * v  # begin selection
 * y  # end selection/copy
 * <prefix> p # paste

and if one also wants to go into the copy mode without `[`, but with `ESC` one should add 

```
bind Escape copy-mode
```

to the `.tmux.conf`

Further reading
---------------
[Arch Wiki](https://wiki.archlinux.org/index.php/Tmux#Vim_friendly_configuration) on tmux and a question similar to mine at [superuser](https://superuser.com/questions/196060/selecting-text-tmux-copy-mode).
And to give some further reedings: A good [Cheet cheet](https://gist.github.com/MohamedAlaa/2961058) on github.
