Title: bash-default-variables
Author: Julian
Date: 2017-03-10
Modified: 2017-03-10
Tags: bash, variables, default, in-a-nutshell, sh
Lang: de
Status: published

Default-Variable
================

Auf den beiden großartigen englischsprachigen Seiten zur Bash-Prgorammierung [tldp](http://www.tldp.org/LDP/abs/html/parameter-substitution.html) und [cyberciti](https://bash.cyberciti.biz/guide/Default_shell_variables_value) kann man jede Menge Tricks zur Variablenmanipulation in bash finden.

Ein sehr häufiger Fall ist die Default-Variable.
Dabei gibt es zwei verschieden Fälle.
Der mit dem Gleichheitszeichen (`=`), mit integrierter Zuweisung des Wertes
``` bash
echo ${varname:="default value"}
```
Hier hat `$varname` den Wert `default value` falls es zuvor nicht schon einen Wert hatte. 

Der andere Fall ist der ohne Zuweisung und dem Minus (`-`), der Beispielsweise sehr interessant ist, wenn man auf die an das Skript übergebenen Argumente (`$1` usw.) verwenden möchte oder einen Defaultwert
``` bash
echo "${1:-'Kein Argument'} übergeben"
```

