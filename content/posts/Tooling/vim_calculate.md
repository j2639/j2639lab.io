Title: Calculate im Vim
Date: 2017-02-28
Author: Julian
Category: Tooling
Tags: vim, in-a-nutshell, xargs, calc
Lang: en
Status: published

If one wants to calculate in `vim` one simply has to write the following line:
```
2 pi mult
```
and visually select it with `V` then pass it to [rpnl](http://www.aps.anl.gov/Accelerator_Systems_Division/Accelerator_Operations_Physics/software.shtml) or `calc` or `bc` or so via `xargs`
```
:!xargs rpnl
```
to get the result in the line.

This trick also works with other commands, like e. g. writing
```
"from math import sin; print(sin(2 + 3))"
``` 
and then run
```
:!xargs python -c 
```
and the line will be replaced with `-0.958924274663`.
