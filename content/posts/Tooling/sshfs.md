Title: sshfs
Author: Julian
Date: 2017-03-10
Modified: 2017-03-10
Tags: ssh, sshfs, mount, network, in-a-nutshell
Lang: en
Status: published

Mount a folder remotely
=======================

I like sshfs the secure shell filesystem. But sometimes I forget how the unmounting command is, so I write this short note to myself.

The mounting part is intuitive:
``` bash
sshfs HOST:PATH mountpoint
```
It mounts the given path from the given host to the given mountpoint. The cool thing is, it needs not to be a partition, but can be any path.

On the other hand, to unmount one needs to remember the `fuser` and the `-u` option in
``` bash
fusermount -u mountpoint
```

fusermount is the filesystem in userspace (FUSE) mounting command and since sshfs is a FUSE filesystem one needs to use its commands and therefore the `-u` for unmounting.
