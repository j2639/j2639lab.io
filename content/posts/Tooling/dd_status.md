Title: dd-progress
Author: Julian
Date: 2017-08-07
Modified: 2017-08-07
Tags: dd, linux, unix, status, in-a-nutshell, pv
Summary: Get the progress of the `dd` command
Lang: en
Status: published

Good old method
===============

Under Linux use
``` bash
sudo kill -USR1 $(pgrep "^dd")
```

Under BSD/OS X/macos
``` bash
sudo kill -INFO $(pgrep "^dd")
```


New GNU coreutils' method
=========================

Newer GNU `dd`s (8.24+) have got an additional option
```
dd if=/dev/urandom of=/dev/null status=progress
```


Using `pv`
==========

More advanced with the `pv` command and right from the beginning
```
sudo dd if=/dev/sdb | pv -s 2G | dd of=DriveCopy1.dd bs=4096
```
via [askubuntu](https://askubuntu.com/questions/215505/how-do-you-monitor-the-progress-of-dd)
