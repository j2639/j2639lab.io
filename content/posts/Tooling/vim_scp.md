Title: vimdiff remote files
Author: Julian
Date: 2017-07-11
Modified: 2017-07-30
Tags: vim, scp, ssh, diff, vimdiff, in-a-nutshell
Summary: vimdiff a scp://host//a
Lang: en
Status: published

One feature of vim is that it can be used in terminals on remote servers without X11 forwarding etc.
It has got also a diff tool included which can also act on files at different computers.
To diff a local and a remote file one can use vimdiff with the following arguments
``` bash
vimdiff local_file.txt scp://remotehost//absolute/path/to/file.txt
```
Mind the two slashes after the host, but there is no relative path option.

Unfortunately this does not work with directories like vimdiff does for local directories.
