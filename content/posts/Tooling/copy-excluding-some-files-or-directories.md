Title: blog/content/Tooling/copy-excluding-some-files-or-directories
Author: Julian
Date: 2017-08-08
Modified: 2017-08-08
Tags:
Summary:
Lang: en
Status: draft

Bash
====
via [stackoverflow](https://stackoverflow.com/questions/2193584/copy-folder-recursively-excluding-some-folders) [answers](https://stackoverflow.com/questions/4585929/how-to-use-cp-command-to-exclude-a-specific-directory)
```
shopt -s extglob
cp -R !(dir1|dir2) path/to/destination
```
or for files
```
shopt -s extglob
cp images/!(*.jpg) destination
```

zsh
===

```
setopt EXTENDED_GLOB
cp images/*~*.jpg destination
```

rsync
=====
This of course needs rsync to be installed
```
rsync -av --exclude='path1/to/exclude' --exclude='path2/to/exclude' source destination
```
