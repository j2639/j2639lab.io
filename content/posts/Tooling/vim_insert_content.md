Title: Insert special content like filenames and dates in vim
Author: Julian
Date: 2017-08-06
Modified: 2017-08-06
Tags: vim, filename, date, bash
Summary:
Lang: en
Status: draft

Insert the filename
===================

To insert the filename you opened you can type the following in insert mode
```
<C-R>%
```.
This pastes the special buffer `%` which is the filename.

Or if you are already in the directory you opended your file and want to have the absolute path
```
<C-R>=...
```
Here the vim-command `...` after the `=` is exectuded.

```
<C-R>=system("echo $PWD")
``` 

`<C-R>` means you should press `control`-key and `R`-key in parallel.

