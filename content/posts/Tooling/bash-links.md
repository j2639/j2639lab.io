Title: bash-links
Author: Julian
Date: 2017-08-06
Modified: 2017-08-06
Tags: bash, linklist, devop, unittests, linter, boilerplate, glitch, links
Summary: Links to good resources about bash and shell scripting in general
Lang: en
Status: published

Tips for avoiding typical errors:
=================================

[Online shellcheck](http://www.shellcheck.net/) or the command line version via "dnf install ShellCheck": Checks shell script for different shells and gives hints on avoiding errors in your script.

[Bash3boilerplate](http://bash3boilerplate.sh/) Starting point for new scripts and source of best practices

Bash does behave different if being called as `sh` (though I think it does not behave as sh/bourne shell). So there is a difference between `#!/bin/sh` and `#!/bin/bash` even if `/bin/sh` is a link to `/bin/bash`. [zsh](https://zsh.org) on the other hand behaves like `sh` when it is called via `#!/bin/sh`.

[Argbash](https://fedoramagazine.org/improve-bash-scripts-argbash/) is a tool for generating minimal code for argument parsing of your script.


Good articles
=============

* A very good article about some good practices when bash scripting, including [unittesting, `--help`-options, debugging and `trap`](https://dev.io/thiht/shell-scripts-matter) [google cached](https://webcache.googleusercontent.com/search?q=cache:P_PUcbXAQ_EJ:https://dev.to/thiht/shell-scripts-matter+&cd=1&ct=clnk).
```
cleanup() {
   # blah
}
trap cleanup EXIT
```


Resources
=========

* [Bash-hacker's bash-wiki](http://wiki.bash-hackers.org/start)
* [Cyberciti.biz's bash tutorial/wiki](https://bash.cyberciti.biz/guide/Main_Page)
* [TLDP's bash howto](http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html)
* [Community Bash Style Guide](https://github.com/azet/community_bash_style_guide)
* [Beautiful Bash slides](https://www.slideshare.net/a_z_e_t/inpresentation)
