Title: Tooling/podman
Author: Julian
Date: 2021-10-06
Modified: 2021-10-06
Tags: podman, docker, ldap, root-less
Summary: Run Podman root-less with LDAP on Fedora
Lang: en
Status: draft

Root-less Podman with LDAP on Fedora

Install podman
```
sudo dnf install podman
```

Add your user account name to `/etc/subuid` and `/etc/subgid` by copying the first line and changing the name
(here sg7149 is the user account, we are sure that there are no UIDs in the range between 100000 and 165536):
```
cat <<EOF
sg7149:100000:65536
EOF
```
If your system is used by multiple podman users, you probably want to shift the UID and GID ranges so that they do not overlap, e.g. 
```
user1:100000:65536
user2:200000:65536
```

Make podman aware of the changes (for each user individually)
```
podman system migrate
```
